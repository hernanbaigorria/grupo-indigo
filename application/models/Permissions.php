<?php
class Permissions extends CI_Model 
{

    public function __construct() {}

    // Controla que exista el permiso para esta operacion. De lo contrario, crea el registro y lo setea en false.
    public function get($id_user, $permission_group, $permission_item)
    {
        $conditions['id_user']           = $id_user;
        $conditions['permission_group']  = $permission_group;
        $conditions['permission_item']   = $permission_item;

        $this->db->where($conditions);
        $result = $this->db->get('permissions');

        if ($result->num_rows() > 0)
        {
            // Existe el registro para este permiso.
            $result = $result->result_array();
            return $result[0]['permission_value'];
        }
        else
        {
            // Obtenemos el valor por defecto (gestorp_permissions)
            $default_permissions = $this->config->item('permissions');
            if (isset($default_permissions[$permission_group][$permission_item]))
                $permission_value = $default_permissions[$permission_group][$permission_item];
            else
            {
                if ($permission_group == 'gestorp')
                    $permission_value = DEFAULT_PERMISSION_VALUE;
                
                if ($permission_group == 'pages')
                    $permission_value = DEFAULT_PERMISSION_PAGE_VALUE;

                if ($permission_group == 'modules')
                    $permission_value = DEFAULT_PERMISSION_MODULE_VALUE;
            }
            // Lo guardamos.
            $this->set($id_user, $permission_group, $permission_item, $permission_value);
            return $permission_value;
        }
    }

    // Establece un permiso para el usuario. Si el permiso ya existe, lo actualiza.
    public function set($id_user, $permission_group, $permission_item, $permission_value)
    {
        $conditions['id_user']           = $id_user;
        $conditions['permission_group']  = $permission_group;
        $conditions['permission_item']   = $permission_item;

        $this->db->where($conditions);
        $result = $this->db->get('permissions');

        if ($result->num_rows() > 0)
        {
            // Existe el registro para este permiso.
            // Entonces lo actualizamos.
            $result         = $result->result_array();
            $data['permission_value'] = $permission_value;

                        $this->db->where($conditions);
            $result =   $this->db->update('permissions', $data);

            if ($result == TRUE)
                return (bool)$this->db->affected_rows();

            return FALSE;
        }
        else
        {
            $new_permission                     = $conditions;
            $new_permission['permission_value'] = $permission_value;
            $new_permission['permission_date']  = date('Y-m-d H:i:s');
            $result =   $this->db->insert('permissions', $new_permission);

            if ($result == TRUE)
                return (bool)$this->db->affected_rows();

            return FALSE;
        }
    }

    // Retorna todos los permisos del usuario.
    public function get_user_permissions($id_user)
    {
        // Cargamos la estructura de permisos de GestorP (Los necesarios para utilizar el panel.)
        $permissions            = $this->config->item('permissions');
        
        // Lo complementamos con los permisos de paginas.
        $permissions['pages']   = $this->pages->get_regulable_pages();

        // Agregamos los permisos de modulos.
        $gtp_modules = get_modules();
        $permissions['modules'] = array();
        foreach ($gtp_modules as $key => $module) 
            $permissions['modules'][$module] = 1;

        // Obtenemos todos los permisos del usuario
                             $this->db->where('id_user', $id_user);
        $saved_permissions = $this->db->get('permissions')->result_array();

        // Anexamos cada permiso guardado, a la tabla de permisos del usuario.
        foreach ($saved_permissions as $key => $permission) 
        {
            $p_group    = $permission['permission_group'];
            $p_item     = $permission['permission_item'];
            $p_value    = $permission['permission_value'];

            if ($p_group == 'gestorp')
                $permissions[$p_group][$p_item] =  $p_value;
            
            if ($p_group == 'pages')    // Para este tipo de entidad es compuesto.
                $permissions[$p_group][$p_item]['value'] =  $p_value;

            if ($p_group == 'modules')
                $permissions[$p_group][$p_item] =  $p_value;
        }
        return $permissions;
    }   

    // Elimmina todos los registros de permisos, de una determinada entidad.
    public function clean_permissions($p_group, $p_item)
    {
        $cond['permission_group']   = $p_group;
        $cond['permission_item']    = $p_item;

        $this->db->where($cond);
        return $this->db->delete('permissions');
    }
}
