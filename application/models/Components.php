<?php
class Components extends CI_Model 
{
    
    public function __construct() {
    }

    // INTERACCIONES CON COMPONENTES DE TEMAS

        // Retorna un listado de los componentes disponibles.
        public function get_avalilable_components()
        {
            $components_path   = PATH_THEME_CODE.'components';

            // Tenemos un layout por carpeta, asique buscamos las carpetas.
            $components = glob($components_path . '/*' , GLOB_ONLYDIR);

            // Limpiamos las rutas, porque glob nos da la ruta completa del directorio.
            foreach ($components as $key => $layout) 
            {
                $layout         = explode('/', $layout);
                if (isset($layout[1]))
                    $components[$key]  = $layout[count($layout) - 1];   // tomamos solo el nombre del directorio.
            }

            return $components;
        }

        // Retorna un listado de los componentes disponibles ANIDADES.
        public function get_nested_avalilable_components()
        {
            $components = $this->get_avalilable_components();
            $clean 		= array();

            foreach ($components as $_kcomponent => $_component) 
            {
            	$cp_prefix = strtok($_component, '_');

            	if (strpos($_component, '_') !== FALSE)
            		$clean[$cp_prefix][] = $_component;
            	else
            		$clean[] = $_component;
            }

            return $clean;
        }

    // INTERACCIONES CON COMPONENTES DE PAGINAS.

        public function new_component($id_page, $page_version, $id_parent_component = FALSE, $th_component)
        {
            $data['id_page']                = $id_page;
            $data['page_version']           = $page_version;
            $data['id_parent_component']    = (int)$id_parent_component;
            $data['th_component']           = $th_component;
            $data['component_order']        = $this->get_next_order($id_page, $page_version);
            $data['component_creation_date'] = date('Y-m-d H:i:s');

            $result = $this->db->insert('page_components', $data);

            if ($result == TRUE) 
                return $this->db->insert_id();
            else
                return FALSE; 
        }

        public function upd_component($id_component = FALSE, $id_page, $page_version, $id_parent_component, $th_component, $component_order, $component_creation_date)
        {
            if ($id_component === FALSE) return FALSE;

            $data['id_page']                = $id_page;
            $data['page_version']           = $page_version;
            $data['id_parent_component']    = $id_parent_component;
            $data['th_component']           = $th_component;
            $data['component_order']        = $component_order;
            $data['component_creation_date'] = $component_creation_date;

            $cond['id_component'] = $id_component;

            $this->db->where($cond);
            return $this->db->update('page_components', $data);
        }

        public function get_component($id_component)
        {
            if ($id_component === FALSE) return FALSE;
            $cond['id_component'] = $id_component;

            $this->db->where($cond);
            $result = $this->db->get('page_components');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0];
            }

            return FALSE;
        }

        public function del_component($id_component)
        {
            if ($id_component === FALSE) return FALSE;
            

            // Eliminamos las configuraciones.
            $result = $this->configurations->clean_configurations('component', $id_component);

            if ($result == TRUE)
            {
                // Eliminamos el contenido de este componente.
                $this->db->where('id_component', $id_component);
                $result = $this->db->delete('page_components_content');

                // Eliminamos el componente en si.
                $this->db->where('id_component', $id_component);
                $result = $this->db->delete('page_components');
                
                return TRUE;
            }

            return FALSE;
        }

        public function get_components($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20)
        {
            $cond = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $cond[$filter_column] = $filter_value;
                else
                    if ($filter_column !== FALSE)
                        $cond['id_component'] = $filter_column;

            if (count($cond) > 0)
                $this->db->where($cond);

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
                      $this->db->order_by('component_order', 'ASC');
                      $this->db->order_by('component_creation_date', 'DESC');
            $result = $this->db->get('page_components');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result;
            }

            return array();
        }

        public function set_component($id_component, $filter_column = FALSE, $filter_value = FALSE)
        {
            $data = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $data[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $data[$filter_column] = $filter_value;

            $cond['id_component'] = $id_component;

            $this->db->where($cond);
            $result = $this->db->update('page_components', $data);

            return $this->db->affected_rows();
        }

    // HELPERS

        public function get_next_order($id_page, $page_version)
        {
            $this->db->select_max('component_order');
            $this->db->where('id_page', $id_page);
            $this->db->where('page_version', $page_version);

            $result =   $this->db->get('page_components');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0]['component_order'] + 1;
            }

            return 1;
        }

    // Renderiza un componente, o lo retorna como HTML.
        public function render_component($id_component, $lang = FALSE, $version, $editable = FALSE, $return = FALSE)
        {
            // Verificamos que el componente exista en la DB.
                $component          = $this->get_component($id_component);
                if ($component == FALSE) return NULL;
                if ($lang == FALSE)      $lang = get_web_lang();     // Si no especificaron lenguaje, mostramos el lenguaje web seleccionado.

            // Verificamos que el tipo de componente exista.
                $component_folder   = PATH_THEME_CODE.'components'.DIRECTORY_SEPARATOR.$component['th_component'];
                $component_view     = THEME_LOAD_PATH.'components'.DIRECTORY_SEPARATOR.$component['th_component'].DIRECTORY_SEPARATOR.'component';
                
                if (!is_dir($component_folder)) {
                    show_error("Components: No se encuentra el componente: ".$component['th_component']);
                }

            // Cargamos la configuración del componente.
                $data['COMPONENT_CFG']  = $this->configurations->get_configurations('component', $id_component, $lang);
                $data['component']      = $component;

            // Añadimos datos necesarios
                $data['LANG']           = $lang;
                $data['VERSION']        = $version;
                $data['ID_COMPONENT']   = $id_component;
                
            // Cargamos el cuerpo html del componente.
                if ($editable == FALSE)
                    return $this->load->front($component_view, $data, $return);
                else
                {
                    $c_data['BLK_BODY'] = $this->load->front($component_view, $data, TRUE);
                    return $this->load->view('nawglobe/pages/editor/components/block_editor/block_editor_body', $c_data, $return);
                }
        }

}