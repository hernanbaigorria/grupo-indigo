<div class="media_collection_preview" data-source="<?php echo $reference ?>">
	<?php foreach ($collection as $element): ?>
		<img height="34" src="<?php echo media_uri($element, TRUE) ?>" data-id="<?php echo $element ?>" title="<?php echo media_title($element) ?>">
	<?php endforeach ?>
</div>