	<form class="login-form" action="" method="post">
		<h3 class="form-title"><?php echo $this->lang->line('login_admin_access') ?></h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
				<?php echo $this->lang->line('login_write_email_password') ?>
			</span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Email</label>
			<input class="form-control form-control-solid placeholder-no-fix" type="email" autocomplete="on" placeholder="Email" name="email"/>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9"><?php echo $this->lang->line('login_password') ?></label>
			<input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="<?php echo $this->lang->line('login_password') ?>" name="password"/>
		</div>
		<div class="form-actions">
			<button type="submit" class="btn btn-success uppercase"><?php echo $this->lang->line('login_continue') ?></button>
			<a href="javascript:;" id="forget-password" class="forget-password"><?php echo $this->lang->line('login_restore_password') ?></a>
		</div>
	</form>


	<form class="forget-form" action="" method="post" novalidate="novalidate" style="display: none;">
		<h3><?php echo $this->lang->line('login_forget_password') ?></h3>
		<p>
			<?php echo $this->lang->line('login_enter_email_address') ?>
		</p>
		<div class="form-group">
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email">
		</div>
		<div class="form-actions">
			<button type="button" id="back-btn" class="btn btn-default"><?php echo $this->lang->line('general_back') ?></button>
			<button type="submit" class="btn btn-success uppercase pull-right"><?php echo $this->lang->line('general_done') ?></button>
		</div>
	</form>