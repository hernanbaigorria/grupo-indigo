<?php 

	// La variable $RESOURCES esta controlada, para agregarse en el header y footer.
	$RESOURCES = array(
			'header_css' => array(),
			'footer_js' => array()
		);

	$COMPONENTS = array(
			'bar_header' => TRUE,
			'bar_menu' => TRUE,
			'bar_toolbar' => FALSE,
			'breadcum' => FALSE,
			'pre_footer' => TRUE,
			'footer' => TRUE
		);
 ?>