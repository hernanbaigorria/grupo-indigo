<div class="table-responsive">
	<table class="table table-striped table-bordered table-advance table-hover table-vertical-align">
		<thead>
			<tr>
				<th>
					 <?php echo $this->lang->line('users_avatar') ?>
				</th>
				<th>
					 <?php echo $this->lang->line('users_user') ?>
				</th>
				<th class="hidden-xs">
					 <?php echo $this->lang->line('users_registration_date') ?>
				</th>
				<th class="hidden-xs">
					 <?php echo $this->lang->line('users_last_login') ?>
				</th>
				<th class="hidden-xs">
					 <?php echo $this->lang->line('users_login_count') ?>
				</th>
				<th>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($users as $key => $user): ?>
			<tr>
				<td class="text-center col-md-1">
					<img src="<?php echo get_avatar($user['id_user']) ?>" alt="" width="40">
				</td>
				<td>
					 <?php echo $user['user_name'] ?><br>
					 <?php echo $user['user_email'] ?>
				</td>
				<td class="hidden-xs">
					 <?php echo date_to_view($user['user_registration_date'], '/', '-') ?><br>
					 <?php echo time_to_view($user['user_registration_date'], '/', '-') ?>
				</td>
				<td class="hidden-xs">
					 <?php echo date_to_view($user['user_last_login'], '/', '-') ?><br>
					 <?php echo time_to_view($user['user_last_login'], '/', '-') ?>
				</td>
				<td class="hidden-xs text-center">
					 <?php echo $user['user_login_count'] ?>
				</td>
				<td class="col-md-2 text-center">
					<a class="btn default btn-xs red-stripe" href="?action=edit&id=<?php echo $user['id_user']; ?>">
						<?php echo $this->lang->line('general_edit') ?>
					</a>
					<?php if ($this->session->userdata('id_user') !== $user['id_user']): ?>
					<a class="btn red btn-xs red-stripe btn-remove-user" href="javascript:;" data-id_user="<?php echo $user['id_user'] ?>" data-confirmation="<?php echo $this->lang->line('users_delete_sure') ?>">
						<i class="fa fa-trash-o"></i>
					</a>
					<?php endif ?>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>

<?php echo generate_pagination($pagination_current, $pagination_total_items, $pagination_per_page); ?>