<?php $file_info = pathinfo($media['media_file_name']) ?>
<div class="portlet light">
	<div class="portlet-body form">
		<div class="row media">
			<div class="col-md-4">
				<br>
				<ul class="list-unstyled profile-nav">
					<li>
						<?php if (strpos($media['media_type'], 'video') !== FALSE): ?>
							<video src="<?php echo UPLOADS_URL.$media['media_file_name'] ?>" style="width: 100%;" controls></video>
						<?php endif ?>

						<?php if (strpos($media['media_type'], 'image') !== FALSE): ?>
							<img src="<?php echo THUMBNAILS_URL.$media['media_file_name'] ?>" class="img-responsive" alt="<?php echo $media['media_name'] ?>">
						<?php endif ?>

						<?php if (strpos($media['media_type'], 'pdf') !== FALSE): ?>
				            <span class="media-thumbnail text-center" style="display: table;">
				                <i class="fa fa-file-pdf-o" style="font-size: 30px; display: table-cell; vertical-align: middle;"></i>
				            </span>
				        <?php endif ?>

						<?php if (strpos($media['media_file_name'], '.xls') !== FALSE): ?>
				            <span class="media-thumbnail text-center" style="display: table;">
				                <i class="fa fa-file-excel-o" style="font-size: 30px; display: table-cell; vertical-align: middle;"></i>
				            </span>
				        <?php endif ?>

						<?php if (strpos($media['media_file_name'], '.doc') !== FALSE): ?>
				            <span class="media-thumbnail text-center" style="display: table;">
				                <i class="fa fa-file-word-o" style="font-size: 30px; display: table-cell; vertical-align: middle;"></i>
				            </span>
				        <?php endif ?>
					</li>
					<li>
						<?php echo $media['media_file_name'] ?>
					</li>
					<li>
						<strong><?php echo $this->lang->line('media_size'); ?>:</strong> <?php echo $media['media_file_size'] ?> Bytes
					</li>
					<li>
						<strong><?php echo $this->lang->line('media_uploaded'); ?>:</strong> <?php echo $media['upload_date'] ?> 
					</li>
				</ul>
				<br>

			</div>
			<div class="col-md-8">
				
				<form id="frm_update_media" role="form" action="javascript:;">

					<div class="form-group">
						<label class="control-label"><?php echo $this->lang->line('media_name'); ?></label>
						<input name="media_name" type="text" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" value="<?php echo $media['media_name'] ?>" class="form-control">
					</div>
			

					<label class="control-label"><?php echo $this->lang->line('media_file_name'); ?></label>
					<div class="input-group">
						<input required="" name="media_file_name" type="text" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" value="<?php echo $file_info['filename'] ?>" class="form-control">
						<span class="input-group-addon">
							.<?php echo $file_info['extension'] ?>
						</span>
					</div>

					<div class="form-group innerT">
						<label class="control-label"><?php echo $this->lang->line('media_group'); ?></label>
						<select name="media_group" class="form-control">
	                            <option value="0"><?php echo $this->lang->line('media_without_group'); ?></option>
	                        <?php foreach ($media_groups as $g_key => $group): ?>
	                            <option <?php if ($media['media_group'] == $group['id_group']) echo 'selected' ?> value="<?php echo $group['id_group'] ?>"><?php echo $group['group_name'] ?></option>
	                        <?php endforeach ?>
	                    </select>
					</div>

					<div class="form-group">
						<label class="control-label"><?php echo $this->lang->line('media_public_link'); ?></label>
						<input type="text" readonly="1" value="<?php echo base_url(UPLOADS_URL.$media['media_file_name']) ?>" class="form-control" style="cursor: crosshair;">
					</div>

					<div class="form-group">
						<label class="control-label"><?php echo $this->lang->line('media_magic_link'); ?></label>
						<input type="text" readonly="1" value="<?php echo '/M/'.$media['id_media'] ?>" class="form-control" style="cursor: crosshair;">
					</div>

					<input type="hidden" name="id_media" value="<?php echo $media['id_media'] ?>">
					<div class="margin-top-10">
						<?php if (check_permissions('gestorp', 'remove_files')): ?>
							<a href="#" class="btn red btn-delete-media pull-right" data-id_media="<?php echo $media['id_media'] ?>" data-confirmation="<?php echo $this->lang->line('media_delete_sure'); ?>"><?php echo $this->lang->line('general_delete'); ?></a>
						<?php endif ?>
						<a href="?action=gallery" class="btn default"><?php echo $this->lang->line('general_cancel'); ?></a>
						<?php if (check_permissions('gestorp', 'edit_files')): ?>
						<button type="submit" class="btn green">
							<?php echo $this->lang->line('general_save'); ?> 
						</button>
						<?php endif ?>
					</div>
				</form>

			</div>
		</div>

	</div>
</div>

<?php if (check_permissions('gestorp', 'replace_files')): ?>
<div class="portlet light">
	<div class="portlet-body form">
		<form id="frm_replace_media">
			<div class="form-group">
				<label class="control-label"><?php echo $this->lang->line('media_replace_file'); ?> </label>
				<input type="file" name="file_replacement">
				<p class="help-block">
					<?php echo $this->lang->line('media_replace_file_help'); ?> 
				</p>
			</div>
			<input type="hidden" name="id_media" value="<?php echo $media['id_media'] ?>">
			<button type="submit" class="btn green">
				<?php echo $this->lang->line('media_replace_file'); ?> 
			</button>
		</form>
	</div>
</div>
<?php endif ?>