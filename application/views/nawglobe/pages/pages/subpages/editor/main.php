<div class="tabbable tabbable-custom tabbable-noborder ">
	<ul class="nav nav-tabs">

		<?php // Paginas externas no tienen versiones ?>
		<?php if ( ! $page['is_external']): ?>
			<li class="active">
				<a href="#tab-page-versions" data-toggle="tab" aria-expanded="false">
					<?php echo $this->lang->line('pages_page_versions'); ?>
				</a>
			</li>
		<?php endif ?>

		<?php // Opciones definidas por el tema ?>
		<li class="">
			<a href="#tab-page-configuration" data-toggle="tab" aria-expanded="false">
				<?php echo $this->lang->line('pages_page_configuration'); ?>
			</a>
		</li>

		<?php // Adicionales ?>
		<?php if ( ! $page['is_external']): ?>
		<li class="">
			<a href="#tab-page-custom" data-toggle="tab" aria-expanded="false">
				<?php echo $this->lang->line('pages_page_custom'); ?>
			</a>
		</li>
		<?php endif ?>

		<?php // Opciones definidas por el gestor ?>
		<li class="<?php if ($page['is_external']) echo 'active' ?>">
			<a href="#tab-page-general" data-toggle="tab" aria-expanded="false">
				<?php echo $this->lang->line('pages_page_basics'); ?>
			</a>
		</li>

	</ul>
	<div class="tab-content">

		<?php // Paginas externas no tienen versiones ?>
		<?php if ( ! $page['is_external']): ?>
			<div class="tab-pane active" id="tab-page-versions">
				<div class="row">
					<div class="col-md-12">
						<div class="tab-content ">
							<?php load_page_piece('table_versions', array('versions' => $versions))  ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif ?>

		<?php // Opciones definidas por el tema ?>
		<div class="tab-pane" id="tab-page-configuration">
			<div class="row">
				<div class="col-md-12">
					<div class="tab-content innerAll">
						<?php echo generate_multi_configuration_form('page', $page['id_page']); ?>
					</div>
				</div>
			</div>
		</div>

		<?php // Adicionales ?>
		<?php if ( ! $page['is_external']): ?>
		<div class="tab-pane" id="tab-page-custom">
			<div class="row">
				<div class="col-md-12">
					<div class="tab-content innerAll">
						<form id="frm_update_custom_code" role="form" action="javascript:;">
							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('pages_page_custom_css'); ?></label>
								<textarea class="css_editor" name="page_custom_css" type="text" rows="6" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" class="form-control"><?php echo $page['page_custom_css'] ?></textarea>
							</div>

							<div class="form-group">
								<label class="control-label"><?php echo $this->lang->line('pages_page_custom_js'); ?></label>
								<textarea class="js_editor" name="page_custom_js" type="text" rows="6" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" class="form-control"><?php echo $page['page_custom_js'] ?></textarea>
							</div>

							<input type="hidden" name="id_page" value="<?php echo $page['id_page'] ?>">
							<button type="submit" class="btn green">
								<?php echo $this->lang->line('general_save'); ?> 
							</button>
						</form>

					</div>
				</div>
			</div>
		</div>
		<?php endif ?>

		<?php // Opciones definidas por el gestor ?>
		<div class="tab-pane <?php if ($page['is_external']) echo 'active' ?>" id="tab-page-general">
			<div class="row">
				<div class="col-md-12">
					<div class="tab-content innerAll">
						<div id="tab-page-info" class="tab-pane active">
							<form id="frm_update_page" role="form" action="javascript:;">

								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('pages_page_name'); ?></label>
									<input required name="page_title" value="<?php echo $page['page_title'] ?>" type="text" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" class="form-control">
								</div>

								<div class="form-group">
									<label><?php echo $this->lang->line('pages_page_slug'); ?></label>
									<?php if ($page['is_external']): ?>
										<div class="input-group">
											<span class="input-group-addon">
												URL
											</span>
											<input name="page_slug" value="<?php echo $page['page_slug'] ?>" type="text" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
										</div>
									<?php else: ?>
										<div class="input-group">
											<span class="input-group-addon">
												<?php echo base_url() ?><span id="spn-section-uri" class="hidden-section hidden-simple-page"><?php if ($page['id_parent_page'] != 0) echo $page['parent_page']['page_slug'].'/' ?></span>
											</span>
											<input name="page_slug" value="<?php echo $page['page_slug'] ?>" type="text" class="form-control input-slug" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
											<span class="input-group-addon hidden-section-page hidden-simple-page <?php if ($page['is_section'] == FALSE) echo 'hidden' ?>">
												<?php echo $this->lang->line('pages_spage_uri'); ?>
											</span>
										</div>
									<?php endif ?>
								</div>

								<?php if (!$page['is_external']): ?>
								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('pages_page_cache'); ?></label>
									<select name="page_cache_time" class="form-control">
										<option <?php if ($page['page_cache_time'] == '-1') echo 'selected' ?> value="-1"><?php echo $this->lang->line('pages_page_cache_disabled'); ?></option>
										<option <?php if ($page['page_cache_time'] == '0') echo 'selected' ?> value="0"><?php echo $this->lang->line('pages_page_cache_site'); ?></option>
										<option <?php if ($page['page_cache_time'] == '1') echo 'selected' ?> value="1">1 <?php echo $this->lang->line('pages_page_cache_minutes'); ?></option>
										<option <?php if ($page['page_cache_time'] == '5') echo 'selected' ?> value="5">5 <?php echo $this->lang->line('pages_page_cache_minutes'); ?></option>
										<option <?php if ($page['page_cache_time'] == '10') echo 'selected' ?> value="10">10 <?php echo $this->lang->line('pages_page_cache_minutes'); ?></option>
										<option <?php if ($page['page_cache_time'] == '20') echo 'selected' ?> value="20">20 <?php echo $this->lang->line('pages_page_cache_minutes'); ?></option>
										<option <?php if ($page['page_cache_time'] == '30') echo 'selected' ?> value="30">30 <?php echo $this->lang->line('pages_page_cache_minutes'); ?></option>
										<option <?php if ($page['page_cache_time'] == '60') echo 'selected' ?> value="60">60 <?php echo $this->lang->line('pages_page_cache_minutes'); ?></option>
										<option <?php if ($page['page_cache_time'] == '120') echo 'selected' ?> value="120">2 <?php echo $this->lang->line('pages_page_cache_hours'); ?></option>
										<option <?php if ($page['page_cache_time'] == '360') echo 'selected' ?> value="360">6 <?php echo $this->lang->line('pages_page_cache_hours'); ?></option>
										<option <?php if ($page['page_cache_time'] == '720') echo 'selected' ?> value="720">12 <?php echo $this->lang->line('pages_page_cache_hours'); ?></option>
										<option <?php if ($page['page_cache_time'] == '1440') echo 'selected' ?> value="1440">24 <?php echo $this->lang->line('pages_page_cache_hours'); ?></option>
									</select>
								</div>

								<div class="form-group">
									<label class="control-label"><?php echo $this->lang->line('pages_page_layout'); ?></label>
									<input disabled name="th_layout" value="<?php echo ($page['id_parent_page'] == 0) ? $page['th_layout'] : $page['parent_page']['th_layout'] ?>" type="text" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" class="form-control">
								</div>
								<?php endif ?>

								<div class="margin-top-10">
									<input type="hidden" name="id_page" value="<?php echo $page['id_page'] ?>">
									<input type="hidden" name="id_parent_page" value="<?php echo $page['id_parent_page'] ?>">
									<a href="<?php echo base_url().GESTORP_MANAGER ?>/pages" class="btn default">
										<?php echo $this->lang->line('general_cancel'); ?> 
									</a>
									<button type="submit" class="btn green">
										<?php echo $this->lang->line('general_save'); ?> 
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>