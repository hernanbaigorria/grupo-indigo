<?php 
	// Filtramos las paginas sobre las cuales no tenemos permisos de edicion.
	foreach ($sections as $key => $section) {
		foreach ($section['childs'] as $skey => $child) {
			if (!check_permissions('pages', $child['id_page'])) {
				unset($sections[$key]['childs'][$skey]);
			}
		}
	}
?>

<?php foreach ($sections as $key => $section): ?>
	<?php if (check_permissions('pages', $section['id_page']) OR count($section['childs']) > 0): ?>
		<div class="portlet box blue-hoki marginB">
			<div class="portlet-title">
				<div class="tools pull-left">
					<a href="javascript:;" class="expand page-<?php echo $section['id_page'] ?>" data-original-title="" title=""></a>
				</div>
				<div class="caption cursor-pointer" onclick="$('.expand.page-<?php echo $section['id_page'] ?>').click();" style="width: 97%;">
					&nbsp;&nbsp;<?php echo $section['page_title'] ?> (<?php echo count($section['childs']) ?>)

					<a class="btn btn-xs btn-success pull-right" href="?action=new&preset=grouped&parent=<?php echo $section['id_page'] ?>">
						<?php echo $this->lang->line('pages_create_subpage') ?>
					</a>

				</div>
			</div>
			<div class="portlet-body" style="display: none;">

				<?php load_page_piece('section_header', array('page' => $section))  ?>

				<?php if (count($section['childs']) > 0): ?>
					<?php load_page_piece('section_body', array('pages' => $section['childs'], 'section_slug' => $section['page_slug']))  ?>
				<?php endif ?>

				<?php if (count($section['childs']) == 0): ?>
					<p class="text-center">
						<a class="btn btn-xs btn-success" href="?action=new&preset=grouped&parent=<?php echo $section['id_page'] ?>">
							<?php echo $this->lang->line('pages_create_subpage') ?>
						</a>
						<a class="btn btn-xs red red-stripe btn-remove-page" href="javascript:;" data-id_page="<?php echo $section['id_page'] ?>" data-confirmation="<?php echo $this->lang->line('pages_delete_sure') ?>">
							<?php echo $this->lang->line('pages_delete_section') ?>
						</a>
					</p>
				<?php endif ?>

			</div>
		</div>
	<?php endif ?>
<?php endforeach ?>