<!-- /.modal -->
<div class="modal fade bs-modal-md" id="modal-config-page" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo $this->lang->line('general_config'); ?></h4>
			</div>
			<div class="modal-body">
				<?php echo generate_localized_configuration_form('page', $id_page, $lang) ?>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

