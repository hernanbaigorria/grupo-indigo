<div class="page_component" data-id="<?php echo $component['id_component'] ?>" data-type="<?php echo $component['th_component'] ?>" data-lang="<?php echo $LANG ?>">
	<div class="page_component_header">
		<h5 class="margin-none pull-left component-dragger">
			<i class="fa fa-arrows"></i>
			<?php echo strtoupper(implode(' ', explode('_', $component['th_component'])))  ?>
		</h5>
		<a href="javascript:;" class="gbtn gbtn-danger gbtn-xs btn-remove-component pull-right" data-confirmation="<?php echo $this->lang->line('general_delete_sure'); ?>"><?php echo $this->lang->line('general_delete'); ?></a>
		<a href="javascript:;" class="gbtn gbtn-warning gbtn-xs btn-edit-component pull-right" ><?php echo $this->lang->line('general_config'); ?></a>
	</div>
	<div class="page_component_body">
		<?php echo $BLK_BODY; ?>
	</div>
</div>