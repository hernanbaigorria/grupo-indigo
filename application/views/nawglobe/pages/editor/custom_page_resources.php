<?php 

	// La variable $RESOURCES esta controlada, para agregarse en el header y footer.
	$RESOURCES = array(
			'header_css' => array(
				'pages/css/gtp_screen_editor.css',
				),
			'footer_js' => array(
				'pages/js/gtp_screen_editor.js',
				'pages/js/gtp_pages.js',
				)
		);

	$COMPONENTS = array(
			'bar_header' => TRUE,
			'bar_menu' => TRUE,
			'bar_toolbar' => FALSE,
			'breadcum' => FALSE,
			'pre_footer' => TRUE,
			'footer' => FALSE
		);
 ?>