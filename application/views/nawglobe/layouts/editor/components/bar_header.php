		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="<?php echo base_url() ?>" target="_blank" class="text-center logo-default" style="background-image: url('<?php echo nwg_assets_theme('nawglobe/default_logo.png') ?>')">
			</a>
		</div>
		<!-- END LOGO -->

		<!-- BEGIN PAGE ACTIONS -->
		<div class="page-actions" style="width: calc(100% - 290px);">

			<!-- IDIOMAS -->
			<div class="btn-group <?php if (count($available_languages) == 1) echo 'hidden' ?>">
				<button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<span class="hidden-sm hidden-xs"><?php echo $this->lang->line('general_currently_editing') ?>: <?php echo $lang ?> &nbsp;</span><i class="fa fa-angle-down"></i>
				</button>
				<ul class="dropdown-menu" role="menu">
					<?php foreach ($available_languages as $a_lang): ?>
					<?php if ($a_lang != $lang): ?>
					<li>
						<a href="<?php echo generate_get_string('lang', $a_lang) ?>">
							<?php echo $this->lang->line('general_edit') ?>: <?php echo $a_lang ?>
						</a>
					</li>
					<?php endif ?>
					<?php endforeach ?>
				</ul>
			</div>
			<!-- IDIOMAS -->

			<!-- Bloques -->
			<div class="btn-group">
				<button type="button" class="btn red-haze btn-sm dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<span class="hidden-sm hidden-xs"><i class="icon-layers"></i> &nbsp;</span><i class="fa fa-angle-down"></i>
				</button>
				<ul class="dropdown-menu" role="menu">
					<?php foreach ($avalilable_components as $component => $child_components): ?>
						<?php if (is_array($child_components)): ?>
							<li class="dropdown-submenu">
								<a href="javascript:;">
									<small><?php echo text_preview(strtolower(implode(' ', explode('_', $component))), 25, '...') ?></small>
								</a>
								<ul class="dropdown-menu" role="menu">
									<?php foreach ($child_components as $_kcmp => $_component): ?>
										<li><a class="btn-add-block" href="javascript:;"  data-component="<?php echo $_component ?>" title="<?php echo strtolower(implode(' ', explode('_', $_component))) ?>"><?php echo $_component ?></a></li>
									<?php endforeach ?>
								</ul>
							</li>
						<?php else: ?>
							<li>
								<a href="javascript:;" class="btn-add-block" data-component="<?php echo $child_components ?>" title="<?php echo strtolower(implode(' ', explode('_', $child_components))) ?>">
									<small><?php echo text_preview(strtolower(implode(' ', explode('_', $child_components))), 25, '...') ?></small>
								</a>
							</li>
						<?php endif ?>
					<?php endforeach ?>
				</ul>
			</div>

			<!-- Bloques -->

			<!-- PREVIEW -->
			<div class="btn-group">
				<a target="_blank" class="btn btn-sm blue btn-config-page" title="<?php echo $this->lang->line('pages_page_configuration'); ?>"> <i class="icon-wrench"></i></a>
			</div>
			<!-- PREVIEW -->

			<!-- PREVIEW -->
			<div class="btn-group">
				<a href="<?php echo base_url().GESTORP_MANAGER ?>/page_preview<?php echo generate_get_string() ?>" target="_blank" class="btn btn-sm green" title="<?php echo $this->lang->line('general_preview'); ?>"><i class="icon-eye"></i></a>
			</div>
			<!-- PREVIEW -->

			<!-- CERRAR -->
			<div class="btn-group pull-right">
				<a href="javascript:;" onclick="window.top.close();" class="btn btn-sm purple"><?php echo $this->lang->line('general_close'); ?></a>
			</div>
			<!-- CERRAR -->

		</div>
		<!-- END PAGE ACTIONS -->