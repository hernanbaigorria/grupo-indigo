<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_editor extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        if(!$this->input->is_ajax_request()) 
			show_404();

        $this->lang->load('pages_lang', $this->session->userdata('user_language'));
        $this->lang->load('general_lang', $this->session->userdata('user_language'));
    }

    // Llmado desde gestorp
    // Devuelve un modal con las opciones de configuracion de la pagina.
    public function get_modal_page_configuration()
    {
        $id_page        = $this->input->post('id_page');
        $lang           = $this->input->post('lang');

        // CONTROLES
            // Controlamos la sesion.
            if ($this->session->userdata('logged_in') != 1) 
                ajax_response('success', $this->lang->line('general_ns_must_login_again'), 0, 1);

            // Controlamos que el usuario tenga permiso para alterar paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

            // Controlamos que el usuario tenga permiso de gestionar esta pagina.
            check_permissions('pages', $id_page, FALSE, TRUE);
        // FIN CONTROLES

		// Armamos la respuesta en JSON
		$data['id_page'] 		= $id_page;
		$data['lang'] 			= $lang;
		$ext['composer'] = $this->load->view('nawglobe/pages/editor/components/modal_config_page', $data, TRUE);	
		ajax_response('success', 'ok.', 1, 0, $ext);
    }

    // Llmado desde gestorp
    // Agrega un bloque a una pagina.
    public function add_block()
    {
	    $id_page 		= $this->input->post('id_page');
	    $version 		= $this->input->post('version');
	    $th_component 	= $this->input->post('th_component');
    	$lang 			= $this->input->post('lang');

        // CONTROLES
            // Controlamos la sesion.
            if ($this->session->userdata('logged_in') != 1) 
                ajax_response('success', $this->lang->line('general_ns_must_login_again'), 0, 1);

            // Controlamos que el usuario tenga permiso para alterar paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

            // Controlamos que el usuario tenga permiso de gestionar esta pagina.
            check_permissions('pages', $id_page, FALSE, TRUE);
        // FIN CONTROLES
    	
        // Creamos el componente
        $id_component = $this->components->new_component($id_page, $version, FALSE, $th_component, 0);
        // Le aplicamos la configuracion por defecto.
        $this->configurations->reset_configuration('component', $id_component);

    	if ($id_component == FALSE) 
    		ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');

    	ajax_response('success', $this->lang->line('general_everything_ok'), '1', '0');
    }

    // Llamado desde la pagina renderizada en modo edicion.
    // Elimina un bloques.
    public function remove_block()
    {
        $id_component       = $this->input->post('id');
        $component          = $this->components->get_component($id_component);

        // CONTROLES
            // Controlamos la sesion.
            if ($this->session->userdata('logged_in') != 1) 
                ajax_response('success', $this->lang->line('general_ns_must_login_again'), 0, 1);

            if ($component == FALSE) 
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
            
            // Controlamos que el usuario tenga permiso para alterar paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);

            // Controlamos que el usuario tenga permiso de gestionar esta pagina.
            check_permissions('pages', $component['id_page'], FALSE, TRUE);
        // FIN CONTROLES

        // Eliminamos el componente.
        $result = $this->components->del_component($id_component);

        if ($result == FALSE) 
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');

        ajax_response('success', $this->lang->line('general_everything_ok'), '1', '0');
    }

    // Llamado desde la pagina renderizada en modo edicion.
    // Guarda el orden de presentacion de los bloques.
    public function save_blocks_order()
    {
    	$order = $this->input->post('order');
    	$order = explode(',', $order);
        
        // CONTROLES
            // Controlamos la sesion.
            if ($this->session->userdata('logged_in') != 1) 
                ajax_response('success', $this->lang->line('general_ns_must_login_again'), 0, 1);

            // Controlamos que el usuario tenga permiso para alterar paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);
        // FIN CONTROLES

    	// Guardamos el orden de los componentes.
    	if (count($order) > 0) 
    	{
            // Controlamos que al menos el primer elemento perteneza a una pagina con permisos.
            $component = $this->components->get_component($order[0]);
            check_permissions('pages', $component['id_page'], FALSE, TRUE);

            // Guardamos el orden.
    		foreach ($order as $position => $id_component) {
    			$this->components->set_component($id_component, 'component_order', $position);
    		}
    	}

    	ajax_response('success', $this->lang->line('general_everything_ok'), '1', '0');
    }

    // Llamado desde la pagina renderizada en modo edicion.
    // Guarda el el contenido de algunos de los campos del componente.
    public function save_content()
    {
        $id_content   = $this->input->post('id');
        $new_value    = $this->input->post('value');
        $content    = $this->contents->get_content($id_content);
        $component  = $this->components->get_component($content['id_component']);

        // Controlamos la sesion.
        if ($this->session->userdata('logged_in') == 1) 
        {
            // Controlamos que el usuario tenga permiso para alterar paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);
            // Controlamos que el usuario tenga permisos para editar la pagina a la cual pertence este content.
            check_permissions('pages', $component['id_page'], FALSE, TRUE);

            // Actualizamos el registro de este contenido..
            $result = $this->contents->set_content($id_content, 'content_value', $new_value);
            
            // Mostramos el mismo contenido que se guardo.
            echo $new_value;
        }
        else
        {
            // Mostramos el mismo contenido que estaba guarado.
            echo ' ['.$this->lang->line('general_must_login_again').']'.$content['content_value'];   
        }
    }

    // Llamado desde la pagina renderizada en modo edicion.
    // Devuelve el modal con las opciones de configuracion del bloque
	public function get_modal_component_editor()
	{
		$id_component	= $this->input->post('id');
	    $th_component	= $this->input->post('type');
	    $lang			= $this->input->post('lang');

        // CONTROLES
            // Controlamos la sesion.
            if ($this->session->userdata('logged_in') != 1) 
                ajax_response('success', $this->lang->line('general_ns_must_login_again'), 0, 1);

            // Controlamos que el usuario tenga permiso para alterar paginas
            check_permissions('gestorp', 'manage_pages', FALSE, TRUE);
            
            $component  = $this->components->get_component($id_component);
            
            check_permissions('pages', $component['id_page'], FALSE, TRUE);
        // FIN CONTROLES

		// Armamos la respuesta en JSON

        $data['id_component']   = $id_component;
        $data['th_component']   = $th_component;
        $data['lang']           = $lang;
        $ext['composer'] = $this->load->view('nawglobe/pages/editor/components/block_editor/modal_block_cfg', $data, TRUE); 
		
        ajax_response('success', 'ok.', 1, 0, $ext);
	}

}
