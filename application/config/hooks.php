<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['pre_controller'][] = array(
    'class' => '',
    'function' => 'pre_controller',
    'filename' => 'pre_controller.php',
    'filepath' => 'hooks'
);

$hook['post_controller_constructor'][] = array(
    'class' => '',
    'function' => 'post_controller',
    'filename' => 'post_controller.php',
    'filepath' => 'hooks'
);

$hook['pre_system'][] = array(
    'class' => '',
    'function' => 'pre_system',
    'filename' => 'pre_system.php',
    'filepath' => 'hooks'
);
