<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// Titulo de la pagina
	$ADDED_CFG['page_title']['default']								= '';
	$ADDED_CFG['page_title']['multilang'] 	 						= TRUE;
	$ADDED_CFG['page_title']['permissions'] 						= FALSE;
	$ADDED_CFG['page_title']['type'] 								= 'text';
