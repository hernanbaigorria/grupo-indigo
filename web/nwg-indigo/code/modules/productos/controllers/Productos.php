<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends MX_Controller {

	// System 

		private $current_category 	= FALSE;
		private $current_entry 		= FALSE;

	    function __construct() {
	        parent::__construct();
	        $this->load->model('mod_productos_entries');
	        $this->load->model('mod_productos_categories');
	        $this->load->library('session');
	    }


    	function _output($output)
        {
        	if(!$this->input->is_ajax_request()) 
            	echo minify_html($output);
            else
            	echo $output;
        }

		public function _remap( $method )
		{
			$segs 	= $this->uri->segment_array();
	        $params	= array_slice($segs, array_search($method, $segs));

	        // Bloqueamos determinados metodos.
		    if (in_array($method, array('module_managment'))) 
		    	show_404();
		    else
		    	if (method_exists($this, $method))
		    		$this->$method(implode(',', $params));
		    	else {
		    		$cond['category_slug'] 	= $method;

		    		$categories 			= $this->mod_productos_categories->get_categories($cond, FALSE, 0, 500);

		    		if (count($categories)  > 0) {
		    			$this->current_category = $categories[0];

		    			$this->render_category();
		    		}
		    		else
		    			show_404();
		    	}
		}

		

	// Public Access

	    // Pantalla publica principal del modulo.
		public function index()
		{

			$page 	 	= (int)$this->input->get('pagina');
			$page 	 	= ($page > 0) ? $page - 1 : 0;
			
			// Si tenemos alguna pagina indicada por URL, vamos derecho alla, sino mostramos el listado de la categoria.
			$cond['entry_published'] 				= 1;
			$cond['id_entry<='] 					= date('Y-m-d H:i:s');
			$entries 								= $this->mod_productos_entries->get_entries($cond, FALSE, 0, 999999999, array('id_entry' => 'DESC'));
			
			$module_data['entries'] 		= $entries;
			$module_data['categories'] 		= $this->mod_productos_categories->get_categories(FALSE, FALSE, 0, 500);
			$module_data['category'] 		= '';
			$module_data['tipos'] 		= $this->mod_productos_categories->get_tipos(FALSE, FALSE, 0, 500);
			$module_data['pagination_current']		= $page;
			$module_data['pagination_total_items']	= @$module_data['entries'][0]['total_results'];
			$module_data['pagination_per_page']		= 10;

			$data['MODULE_BODY'] 	= $this->load->view('public/main', $module_data, TRUE);
			echo minify_html(render_module('productos', $data));
		}

		public function render_category()
		{

			// Si tenemos alguna pagina indicada por URL, vamos derecho alla, sino mostramos el listado de la categoria.
			$cond['mod_productos_entries.id_category'] 	= $this->current_category['id_category'];
			if ($this->session->userdata('logged_in') != TRUE) $cond['entry_published'] = 1;
			$entries 								= $this->mod_productos_entries->get_entries($cond, FALSE, 0, 10, array('id_entry' => 'DESC'));
			
				$page 	 	= (int)$this->input->get('pagina');
				$page 	 	= ($page > 0) ? $page - 1 : 0;

				$econd['mod_productos_entries.id_category'] 		= $this->current_category['id_category'];
				$econd['entry_published'] 	= 1;
				$entries 					= $this->mod_productos_entries->get_entries($econd, FALSE, $page, 999, array('id_entry' => 'DESC'), $this->input->get('buscar'));

				$module_data['entries'] 		= $entries;
				$module_data['categories'] 		= $this->mod_productos_categories->get_categories(FALSE, FALSE, 0, 500);
				$module_data['category'] 		= $this->current_category;
				$module_data['tipos'] 		= $this->mod_productos_categories->get_tipos(FALSE, FALSE, 0, 500);
				$module_data['pagination_current']		= $page;
				$module_data['pagination_total_items']	= @$module_data['entries'][0]['total_results'];
				$module_data['pagination_per_page']		= 10;
				$data['MODULE_BODY'] = $this->load->view('public/listing', $module_data, TRUE);
				echo minify_html(render_module('productos', $data));
			
		}

		public function render_entry()
		{
			#echo "Se presenta una entrada:";
			#debugger($this->current_entry, 1);

			$module_data['entry'] 		= $this->current_entry;
			$module_data['categories'] 	= $this->mod_productos_categories->get_categories(FALSE, FALSE, 0, 500);

			$data['MODULE_BODY'] = $this->load->view('public/productos', $module_data, TRUE);
			echo minify_html(render_module('productos', $data));
		}

		// Contenido HTML del bloque integrado en componentes.
		public function block_categorias()
		{	

			$data['categories'] 	= $this->mod_productos_entries->categorias_productos();

			// Utilizamos array_filter para filtrar los elementos que cumplen con la condición
			$elementosConTitulo = array_filter($data['categories'], function($elemento) {
			    return !empty($elemento->entry_title);
			});

			// Tomamos el primer elemento del nuevo array (si existe)
			$categoria = reset($elementosConTitulo);


			$cond['entry_published'] 	= 1;
			$cond['mod_productos_entries.id_category'] 	= $categoria->id_category;

			$data['entries'] 			= $this->mod_productos_entries->get_entries($cond, FALSE, 0, 999, array('mod_productos_entries.id_category' => 'ASC'));

			$this->load->view('public/categorias', $data);
		}

	// API

		public function get_categories($cond = FALSE, $value = FALSE, $page = 0, $max = 10, $having = FALSE)
		{
			return $this->mod_productos_categories->get_categories($cond, $value, $page, $max, FALSE, $having);
		}

		public function get_lastest_posts($cond = FALSE, $value = FALSE, $page = 0, $max = 4, $having = FALSE)
		{
			return $this->mod_productos_entries->get_entries($cond, $value, $page, $max, array('id_entry' => 'DESC'), $having);
		}
	// Administracion del modulo.

		public function module_managment()
		{
			$this->control_library->session_check();
			
			// Levantamos variables GET
			$action 	= $this->input->get('action');
			$id_entry 	= $this->input->get('id');
			$page 	 	= (int)$this->input->get('pagina');
			$page 	 	= ($page > 0) ? $page - 1 : 0;

			switch ($action) {
				case 'edit':
					$data['entry'] 		= $this->mod_productos_entries->get_entry($id_entry);
					$data['categories'] = $this->mod_productos_categories->get_categories(FALSE, FALSE, 0, 500);
					$data['tipos'] = $this->mod_productos_categories->get_tipos_editor();
					$this->load->view('manager/entry_editor', $data);
					break;
				case 'new':
					$data['categories'] = $this->mod_productos_categories->get_categories(FALSE, FALSE, 0, 500);

					$data['tipos'] = $this->mod_productos_categories->get_tipos_editor();
					$this->load->view('manager/entry_creator', $data);
					break;
				default:
					$data['entries'] = $this->mod_productos_entries->get_entries(FALSE, FALSE, $page, 10, array('id_entry' => 'DESC'));
					
					$data['pagination_current']		= $page;
					$data['pagination_total_items']	= @$data['entries'][0]['total_results'];
					
					$data['pagination_per_page']	= 10;
					$data['PAGE_BUTTON']['link']	= '?action=new';
					$data['PAGE_BUTTON']['class']	= 'green';
					$data['PAGE_BUTTON']['icon']	= 'fa-plus';
					$data['PAGE_BUTTON']['text']	= 'Nuevo producto';
					
					$this->load->view('manager/main', $data);
					break;
			}
		}

		public function categorias()
		{
			$this->control_library->session_check();
			
			// Levantamos variables GET
			$action 	= $this->input->get('action');
			$id_category= $this->input->get('id');
			$page 	 	= (int)$this->input->get('pagina');
			$page 	 	= ($page > 0) ? $page - 1 : 0;

			switch ($action) {
				case 'edit':
					$data['category'] = $this->mod_productos_categories->get_category($id_category);
					$this->load->view('manager/category_editor', $data);
					break;
				case 'new':
					$this->load->view('manager/category_creator', FALSE);
					break;
				default:
					$data['categories'] = $this->mod_productos_categories->get_categories(FALSE, FALSE, $page, 10, array('id_category' => 'DESC'));
					
					$data['pagination_current']		= $page;
					$data['pagination_total_items']	= @$data['categories'][0]['total_results'];
					
					$data['pagination_per_page']	= 10;
					$data['PAGE_BUTTON']['link']	= '?action=new';
					$data['PAGE_BUTTON']['class']	= 'green';
					$data['PAGE_BUTTON']['icon']	= 'fa-plus';
					$data['PAGE_BUTTON']['text']	= 'Nueva Categoria';
					
					$this->load->view('manager/main_categories', $data);
					break;
			}
		}

		public function variables()
		{
			$this->control_library->session_check();
			
			// Levantamos variables GET
			$action 	= $this->input->get('action');
			$id_variable= $this->input->get('id');
			$page 	 	= (int)$this->input->get('pagina');
			$page 	 	= ($page > 0) ? $page - 1 : 0;

			switch ($action) {
				case 'edit':
					$data['tipos'] = $this->mod_productos_categories->get_tipos(FALSE, FALSE, $page, 10, array('id' => 'DESC'));
					$data['variable'] = $this->mod_productos_categories->get_variable($id_variable);
					$this->load->view('manager/variable_editor', $data);
					break;
				case 'new':
					$data['tipos'] = $this->mod_productos_categories->get_tipos(FALSE, FALSE, $page, 10, array('id' => 'DESC'));
					$this->load->view('manager/variable_creator', $data);
					break;
				default:
					$data['variables'] = $this->mod_productos_categories->get_variables(FALSE, FALSE, $page, 10, array('id' => 'DESC'));
					
					$data['pagination_current']		= $page;
					$data['pagination_total_items']	= @$data['variables'][0]['total_results'];
					
					$data['pagination_per_page']	= 10;
					$data['PAGE_BUTTON']['link']	= '?action=new';
					$data['PAGE_BUTTON']['class']	= 'green';
					$data['PAGE_BUTTON']['icon']	= 'fa-plus';
					$data['PAGE_BUTTON']['text']	= 'Nueva Variable';
					
					$this->load->view('manager/main_variables', $data);
					break;
			}
		}


		public function tipos()
		{
			$this->control_library->session_check();
			
			// Levantamos variables GET
			$action 	= $this->input->get('action');
			$id_tipo= $this->input->get('id');
			$page 	 	= (int)$this->input->get('pagina');
			$page 	 	= ($page > 0) ? $page - 1 : 0;

			switch ($action) {
				case 'edit':
					$data['tipo'] = $this->mod_productos_categories->get_tipo($id_tipo);
					$this->load->view('manager/tipo_editor', $data);
					break;
				case 'new':
					$this->load->view('manager/tipo_creator', FALSE);
					break;
				default:
					$data['tipos'] = $this->mod_productos_categories->get_tipos(FALSE, FALSE, $page, 10, array('id' => 'DESC'));
					
					$data['pagination_current']		= $page;
					$data['pagination_total_items']	= @$data['categories'][0]['total_results'];
					
					$data['pagination_per_page']	= 10;
					$data['PAGE_BUTTON']['link']	= '?action=new';
					$data['PAGE_BUTTON']['class']	= 'green';
					$data['PAGE_BUTTON']['icon']	= 'fa-plus';
					$data['PAGE_BUTTON']['text']	= 'Nuevo Tipo';
					
					$this->load->view('manager/main_tipos', $data);
					break;
			}
		}

		public function ver()
		{	
			$segs 	= $this->uri->segment_array();
			$data['categories'] 	= $this->mod_productos_entries->categorias_productos();

			// Utilizamos array_filter para filtrar los elementos que cumplen con la condición
			$elementosConTitulo = array_filter($data['categories'], function($elemento) {
			    return !empty($elemento->entry_title);
			});

			// Tomamos el primer elemento del nuevo array (si existe)
			$categoria = reset($elementosConTitulo);

			$cond['entry_published'] 	= 1;
			$cond['mod_productos_entries.id_entry'] 	= $segs[3];

			$data['entries'] 			= $this->mod_productos_entries->get_entries($cond, FALSE, 0, 999, array('mod_productos_entries.id_category' => 'ASC'));

			$data['tipos'] = $this->mod_productos_entries->tipos($segs[3]);

			$data['MODULE_BODY'] = $this->load->view('public/producto', $data, TRUE);
			echo minify_html(render_module('productos', $data));


		}

		public function opciones()
		{
			$this->control_library->session_check();
			// El form es generado por el gestor directamente. Las opciones de config estan en config_schema de este modulo.
			$this->load->view('manager/options', FALSE);
		}
}
