<?php 
class Mod_productos_entries extends CI_Model 
{
	
	public function __construct() 
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
	}


	public function tipos($id_producto)
	{
		$this->db->select('variables.*');
		$this->db->select('tipos.nombre as tipo');
		$this->db->where('variables_productos.id_producto', $id_producto);
		$this->db->join('variables', 'variables.id = variables_productos.id_variable');
		$this->db->join('tipos', 'tipos.id = variables.id_tipo');
		$query = $this->db->get('variables_productos');
		return $query->result();
	}

	public function new_entry($id_category, $entry_published, $entry_title, $entry_content, $entry_images, $ancho_total, $ancho_util, $variables)
	{
		$data['id_category'] 	= $id_category;
		$data['entry_published'] = $entry_published;
		$data['entry_title'] 	= $entry_title;
		$data['entry_images'] 	= $entry_images;
		$data['entry_content'] 	= $entry_content;

		$data['ancho_total'] 	= $ancho_total;
		$data['ancho_util'] 	= $ancho_util;

		$result = $this->db->insert('mod_productos_entries', $data);

        if ($result == TRUE):

        	$id_producto = $this->db->insert_id();

        	foreach($variables as $variable):
        		$data = array(
        			'id_producto' => $id_producto,
        			'id_variable' => $variable,
        		);
        		$this->db->insert('variables_productos', $data);
        	endforeach;

            return $id_producto;
        else:
            return FALSE; 
        endif;
	}

	public function upd_entry($id_entry = FALSE, $id_category, $entry_published, $entry_title, $entry_content, $entry_images, $color_01, $color_02, $color_03, $color_04, $color_05)
	{
		if ($id_entry === FALSE) return FALSE;

		$data['id_category'] = $id_category;
		$data['entry_published'] = $entry_published;
		$data['entry_title'] = $entry_title;
		$data['entry_content'] = $entry_content;
		$data['entry_images'] = $entry_images;

		$data['color_01'] 	= $color_01;
		$data['color_02'] 	= $color_02;
		$data['color_03'] 	= $color_03;
		$data['color_04'] 	= $color_04;
		$data['color_05'] 	= $color_05;

		$cond['id_entry'] = $id_entry;

		$this->db->where($cond);
		return $this->db->update('mod_productos_entries', $data);
	}

	public function get_entry($id_entry)
	{
		if ($id_entry === FALSE) return FALSE;
		$cond['id_entry'] = $id_entry;

		$this->db->select('SQL_CALC_FOUND_ROWS mod_productos_categories.*, mod_productos_entries.*', FALSE);
		$this->db->from('mod_productos_entries');
		$this->db->join('mod_productos_categories', 'mod_productos_categories.id_category = mod_productos_entries.id_category');
		$this->db->where($cond);
		$result = $this->db->get();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result[0];
		}

		return FALSE;
	}

	public function del_entry($id_entry)
	{
		if ($id_entry === FALSE) 
			return FALSE;
		
		$cond['id_entry'] = $id_entry;

		$this->db->where($cond);
		$result = $this->db->delete('mod_productos_entries');

		return $this->db->affected_rows();
	}

	public function get_entries($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $search_terms = FALSE) {
	    $cond = array();

	    $catwher 			= $this->mod_productos_categories->get_category_by_slug($this->uri->segment(2));

	    $this->db->select('SQL_CALC_FOUND_ROWS mod_productos_categories.*, mod_productos_entries.*, variables_productos.*, variables.nombre', FALSE);
	    $this->db->from('mod_productos_entries');
	    $this->db->join('mod_productos_categories', 'mod_productos_categories.id_category = mod_productos_entries.id_category');
	    $this->db->join('variables_productos', 'variables_productos.id_producto = mod_productos_entries.id_entry');
	    $this->db->join('variables', 'variables.id = variables_productos.id_variable');

	    // Aplicar condiciones fijas primero
	    if(empty($_GET['pagina'])):
	    if (!empty($_SERVER['QUERY_STRING'])) {
	    	if(!empty($catwher)):
	    		$this->db->where('mod_productos_entries.id_category', $catwher[0]->id_category);
	    	endif;
	    	
	        $query_string = $_SERVER['QUERY_STRING'];
	        parse_str($query_string, $params);

	        $filters = array();
	        $likes = array();

	        $this->db->group_start();

	        if (isset($params['filters_names']) && !empty($params['filters_names'])) {
	            $this->db->or_where_in('mod_productos_entries.id_entry', $params['filters_names']);
	        }

	        foreach ($params as $key => $value) {
	            if (strpos($key, 'filters') === 0 && $key !== 'filters_names') {
	                if (is_array($value)) {
	                    foreach ($value as $v) {
	                        // Manejo de rangos para "PESO EN ONZAS"
	                        if (preg_match('/^(\d+(\.\d+)?)__(\d+(\.\d+)?)_oz$/', $v, $matches)) {
	                            $start = (float) $matches[1];
	                            $end = (float) $matches[3];
	                            $this->db->or_group_start()
	                                     ->where('CAST(REPLACE(REPLACE(variables.nombre, " OZ/YD2", ""), " OZ", "") AS DECIMAL(10,2)) >=', $start)
	                                     ->where('CAST(REPLACE(REPLACE(variables.nombre, " OZ/YD2", ""), " OZ", "") AS DECIMAL(10,2)) <', $end)
	                                     ->group_end();
	                        } else {
	                            $filters[] = $v;
	                        }
	                    }
	                } else {
	                    $filters[] = $value;
	                }
	            }
	            if (strpos($key, 'b') === 0) {
	                $likes[] = $value;
	            }
	        }

	        if (!empty($filters)) {
	            $this->db->or_group_start();
	            foreach ($filters as $filter) {
	                $this->db->or_where('variables_productos.id_variable', $filter);
	            }
	            $this->db->group_end();
	        }

	        if (!empty($likes)) {
	            $this->db->or_group_start();
	            foreach ($likes as $like) {
	                $this->db->or_like('mod_productos_entries.entry_title', $like);
	            }
	            $this->db->group_end();
	        }

	        $this->db->group_end();
	    }

	    $this->db->group_by('variables_productos.id_producto');
		endif;

	    if (is_array($filter_column)) {
	        foreach ($filter_column as $key => $value) {
	            $cond[$key] = $value;
	        }
	    } else if ($filter_column !== FALSE AND $filter_value !== FALSE) {
	        $cond[$filter_column] = $filter_value;
	    } else if ($filter_column !== FALSE) {
	        $cond['id_entry'] = $filter_column;
	    }

	    if (count($cond) > 0) {
	        $this->db->where($cond);
	    }

	    if (is_array($order_by)) {
	        foreach ($order_by as $order_column => $sort_order) {
	            $this->db->order_by($order_column, $sort_order);
	        }
	    }

	    if (!empty($search_terms)) {
	        $this->db->group_start();
	        $this->db->like('entry_title', $search_terms, 'both');
	        $this->db->or_like('entry_content', $search_terms, 'both');
	        $this->db->or_like('entry_short', $search_terms, 'both');
	        $this->db->group_end();
	    }

	    if ($page !== FALSE) {
	        $offset = $page * $page_items;
	        $this->db->limit($page_items, $offset);
	    }

	    $result = $this->db->get();
	    
	    $paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

	    if ($result->num_rows() > 0) {
	        $result = $result->result_array();
	        foreach ($result as $key => $value) {
	            $result[$key]['total_results'] = $paginacion[0]['total_items'];
	        }
	        return $result;
	    }

	    return array();
	}

	public function get_product_names($category = FALSE)
	{
		$this->db->select('mod_productos_entries.id_entry, mod_productos_entries.entry_title');
		$this->db->where('entry_published', 1);
		if(!empty($category)):
			$this->db->where('mod_productos_categories.id_category', $category);
		endif;

		$this->db->group_by('mod_productos_entries.id_entry');

		$this->db->join('mod_productos_entries', 'mod_productos_entries.id_entry = variables_productos.id_producto');
		$this->db->join('mod_productos_categories', 'mod_productos_categories.id_category = mod_productos_entries.id_category');

		$query = $this->db->get('variables_productos');
		return $query->result();
	}


	public function set_entry($id_entry, $filter_column = FALSE, $filter_value = FALSE)
	{
		$data = array();

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				$data[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				$data[$filter_column] = $filter_value;

		$cond['id_entry'] = $id_entry;

		$this->db->where($cond);
		$result = $this->db->update('mod_productos_entries', $data);

		return (bool)$this->db->affected_rows();
	}


	public function categorias_productos()
	{		

		$this->db->select('mod_productos_categories.*');
		$this->db->order_by('mod_productos_categories.id_category', 'ASC');
		$this->db->select('mod_productos_entries.entry_title');
		$this->db->group_by('mod_productos_categories.category_name');
		$this->db->join('mod_productos_entries', 'mod_productos_entries.id_category = mod_productos_categories.id_category', 'left outer');
		$query = $this->db->get('mod_productos_categories');
		return $query->result();
	}
}