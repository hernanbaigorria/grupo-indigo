<?php #debugger($entries) ?>

<?php foreach ($entries as $key => $_entry): ?>

    <div class="recent-post">
        <img src="<?php echo media_uri($_entry['entry_images']); ?>" class="img-responsive" alt="" style="background:url(<?php echo media_uri($_entry['entry_images']); ?>);background-size:cover;background-repeat:no-repeat;background-position:center;height:0;padding-bottom:70px;">
        <div class="recent-content">
            <a href="<?php echo $_entry['url'] ?>" class="date"><?php echo date_to_view($_entry['entry_date']) ?></a>
            <a href="<?php echo $_entry['url'] ?>"><?php echo text_preview($_entry['entry_short'], 100) ?></a>
        </div>
    </div>

<?php endforeach ?>