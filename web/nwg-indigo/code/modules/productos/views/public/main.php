<?php
// Obtener los parámetros GET de la URL actual
$query_string = $_SERVER['QUERY_STRING'];

// Construir la nueva URL con la base URL y los parámetros GET
$new_url = base_url() . 's/pdf/?' . $query_string;
?>

<?php
function group_variables_by_dynamic_ranges($variables, $range_size = 1) {
    $ranges = [];
    $min = null;
    $max = null;

    $values = [];
    foreach ($variables as $variable) {
        $value = floatval(str_replace([' OZ/YD2', ' OZ'], '', strtoupper($variable->nombre)));
        $values[] = ['value' => $value, 'variable' => $variable];
        if ($min === null || $value < $min) $min = $value;
        if ($max === null || $value > $max) $max = $value;
    }

    usort($values, function($a, $b) {
        return $a['value'] <=> $b['value'];
    });

    $current_range_start = $min;
    while ($current_range_start <= $max) {
        $current_range_end = $current_range_start + $range_size;
        $range_key = "{$current_range_start}__{$current_range_end}_oz";
        $display_key = "{$current_range_start} - {$current_range_end} oz";
        $ranges[$range_key] = ['display' => $display_key, 'variables' => []];
        foreach ($values as $item) {
            if ($item['value'] >= $current_range_start && $item['value'] < $current_range_end) {
                $ranges[$range_key]['variables'][] = $item['variable'];
            }
        }
        $current_range_start = $current_range_end;
    }

    return $ranges;
}
?>
<section class="hero-productos" style="background-image:url(<?php echo THEME_ASSETS_URL ?>general/img/categoria-09.png);">
    <div class="w-100">
      <div class="row m-0">
        <div class="col-12 p-0">
          <div class="content-text">
            <h4>PRODUCTOS</h4>
          </div>
        </div>
      </div>
    </div>
    <div class="filter-hero"></div>
</section>
<?php
// Suponiendo que obtienes los nombres de los productos desde una consulta
$product_names = $this->mod_productos_entries->get_product_names(false); // Este método debe devolver una lista de productos con sus nombres e IDs.
?>

<section class="grid-products">
    <div class="w-100">
        <div class="row m-0">
            <div class="col-12 col-md-2">
                <form method="get" class="filters">
                    <div class="box-filter">
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#nombres" aria-expanded="false" aria-controls="nombres">
                            <i class="fas fa-tag"></i>Productos <i class="fas fa-chevron-down"></i>
                        </button>
                        <div class="collapse" id="nombres">
                            <div class="card card-body">
                                <?php foreach($product_names as $product): 
                                    $selected_values = isset($_GET['filters_names']) ? $_GET['filters_names'] : [];
                                ?>
                                    <div class="checkbox-custom-check">
                                        <input 
                                            type="checkbox" 
                                            id="filter_name<?=$product->id_entry?>" 
                                            name="filters_names[]" 
                                            value="<?=$product->id_entry?>" 
                                            <?= (in_array($product->id_entry, $selected_values)) ? 'checked' : '' ?>
                                        >
                                        <label for="filter_name<?=$product->id_entry?>"><?=$product->entry_title?></label>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>

                    <?php foreach($tipos as $key => $tipo): 
                        $variables = $this->mod_productos_categories->get_variables_by_tipo_by_category($tipo['id'], FALSE);
                        $is_card_active = false;

                        $ranges = [];
                        if (strtolower($tipo['nombre']) === 'peso en onzas') {
                            $ranges = group_variables_by_dynamic_ranges($variables);
                        } else {
                            $ranges = [$tipo['nombre'] => ['display' => $tipo['nombre'], 'variables' => $variables]];
                        }

                        foreach ($ranges as $range => $range_data) {
                            foreach ($range_data['variables'] as $variable) {
                                if (isset($_GET['filters' . $tipo['id']]) && in_array($variable->id, $_GET['filters' . $tipo['id']])) {
                                    $is_card_active = true;
                                    break 2;
                                }
                            }
                        }
                    ?>
                        <div class="box-filter">
                            <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#tipo<?=$key?>" aria-expanded="<?= $is_card_active ? 'true' : 'false' ?>" aria-controls="tipo<?=$key?>">
                                <i class="<?=$tipo['icon']?>"></i> <?=$tipo['nombre']?> <i class="fas fa-chevron-down"></i>
                            </button>
                            <div class="collapse <?= $is_card_active ? 'show' : '' ?>" id="tipo<?=$key?>">
                                <div class="card card-body">
                                    <?php if (strtolower($tipo['nombre']) === 'peso en onzas'): ?>
                                        <?php foreach($ranges as $range => $range_data): 
                                            $selected_values = isset($_GET['filters' . $tipo['id']]) ? $_GET['filters' . $tipo['id']] : [];
                                        ?>
                                            <div class="checkbox-custom-check">
                                                <input 
                                                    type="checkbox" 
                                                    id="filter<?=$range?>" 
                                                    name="filters<?=$tipo['id']?>[]" 
                                                    value="<?=$range?>" 
                                                    <?= (in_array($range, $selected_values)) ? 'checked' : '' ?>
                                                >
                                                <label for="filter<?=$range?>"><?=$range_data['display']?></label>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php else: ?>
                                        <?php foreach($ranges as $range => $range_data): 
                                            foreach($range_data['variables'] as $variable): 
                                                $selected_values = isset($_GET['filters' . $tipo['id']]) ? $_GET['filters' . $tipo['id']] : [];
                                        ?>
                                            <div class="checkbox-custom-check">
                                                <input 
                                                    type="checkbox" 
                                                    id="filter<?=$variable->id?>" 
                                                    name="filters<?=$tipo['id']?>[]" 
                                                    value="<?=$variable->id?>" 
                                                    <?= (in_array($variable->id, $selected_values)) ? 'checked' : '' ?>
                                                >
                                                <label for="filter<?=$variable->id?>"><?=$variable->nombre?></label>
                                            </div>
                                        <?php endforeach; endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="col-12 content-button">
                        <button type="button" onclick="clearAllFilters()">Borrar filtros</button>
                        <input type="submit" value="Filtrar">
                    </div>
                </form>
                <?php if(null !== $this->session->userdata('usuario')): ?>
                    <div class="box-pdf">
                        <a href="<?= $new_url ?>" target="_blank">GENERAR PDF <i class="far fa-file-pdf"></i></a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-12 col-md-10">
                <div class="row m-0">
                    <?php foreach ($entries as $key => $_entry):
                        $images = explode(',' , $_entry['entry_images']);
                    ?>
                        <div class="col-12 col-md-3">
                            <a href="<?=base_url()?>productos/ver/<?=$_entry['id_entry']?>/" class="enlace-producto">
                                <div class="image-product" style="background-image: url('<?php echo media_uri($images[0]) ?>');"></div>
                                <div class="content-info">
                                    <h3><?=$_entry['entry_title']?></h3>
                                    <p>Ancho: <?=$_entry['ancho_total']?>cm</p>
                                </div>
                                <div class="conteiner-link">
                                    VER MÁS <i class="fas fa-caret-right"></i>
                                </div>
                            </a>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</section>