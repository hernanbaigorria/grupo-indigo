<?php #debugger($entries) ?>

<section class="hero-productos"></section>
<section class="grid-products">
    <div class="w-100">
        <div class="row m-0">
            <div class="col-12 col-md-2">
                <div class="filters">
                    <div class="box-filter">
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#color" aria-expanded="false" aria-controls="color">
                        <i class="fas fa-palette"></i> Color <i class="fas fa-chevron-down"></i>
                        </button>
                        <div class="collapse" id="color">
                          <div class="card card-body">
                            <a href="#">Todos</a>
                            <a href="#">Peso 01</a>
                            <a href="#">Peso 02</a>
                            <a href="#">Peso 03</a>
                          </div>
                        </div>
                    </div>
                    <div class="box-filter">
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#construccion" aria-expanded="false" aria-controls="construccion">
                        <i class="fas fa-pencil-ruler"></i> Construcción <i class="fas fa-chevron-down"></i>
                        </button>
                        <div class="collapse" id="construccion">
                          <div class="card card-body">
                            <a href="#">Todos</a>
                            <a href="#">Peso 01</a>
                            <a href="#">Peso 02</a>
                            <a href="#">Peso 03</a>
                          </div>
                        </div>
                    </div>
                    <div class="box-filter">
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#peso" aria-expanded="false" aria-controls="peso">
                        <i class="fas fa-balance-scale"></i> Peso <i class="fas fa-chevron-down"></i>
                        </button>
                        <div class="collapse" id="peso">
                          <div class="card card-body">
                            <a href="#">Todos</a>
                            <a href="#">Peso 01</a>
                            <a href="#">Peso 02</a>
                            <a href="#">Peso 03</a>
                          </div>
                        </div>
                    </div>
                    <div class="box-filter">
                        <button class="btn btn-primary" type="button" data-bs-toggle="collapse" data-bs-target="#ancho_total" aria-expanded="false" aria-controls="ancho_total">
                        <i class="fas fa-ruler"></i> Ancho total <i class="fas fa-chevron-down"></i>
                        </button>
                        <div class="collapse" id="ancho_total">
                          <div class="card card-body">
                            <a href="#">Todos</a>
                            <a href="#">Peso 01</a>
                            <a href="#">Peso 02</a>
                            <a href="#">Peso 03</a>
                          </div>
                        </div>
                    </div>
                </div>
                <?php if(null !== $this->session->userdata('usuario')): ?>
                    <div class="box-pdf">
                        <a href="<?=base_url()?>s/pdf/" target="_blank">GENERAR PDF <i class="far fa-file-pdf"></i></a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-12 col-md-10">
                <div class="row m-0">
                    
                    <?php foreach ($entry as $key => $_entry):
                        print_r($entry);
                        //$images = explode(',' , $_entry->entry_images);
                    ?>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</section>