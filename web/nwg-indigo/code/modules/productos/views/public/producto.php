<style type="text/css">
    #carouselExampleIndicators {display:none;}
</style>
<section class="producto">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6">
                <?php $slider_images = explode(',' , $entries[0]['entry_images']) ?>
                <div id="producto" class="carousel slide">
                    <div class="carousel-indicators">
                      <?php foreach ($slider_images as $_kimages=> $_id_images): ?>
                        <button type="button" data-bs-target="#producto" data-bs-slide-to="<?=$_kimages?>" class="<?php if($_kimages == 0): echo "active"; endif;?>"   aria-current="true" aria-label="Slide 1"></button>
                      <?php endforeach; ?>
                    </div>
                    <div class="carousel-inner">
                        <?php foreach ($slider_images as $_kimages=> $_id_images): ?>
                            <div class="carousel-item <?php if($_kimages == 0): echo "active"; endif;?>">
                              <div class="w-100">
                                <img src="<?php echo media_uri($_id_images) ?>" class="img-fluid">
                              </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#producto" data-bs-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#producto" data-bs-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="visually-hidden">Next</span>
                    </button>
                </div>
            </div>
            <div class="col-12 col-sm-6 box-producto">
                <h3><?=$entries[0]['entry_title']?></h3>
                <p>Ancho: <?=$entries[0]['ancho_total']?></p>
                <?php foreach($tipos as $tipo): ?>
                    <p><?=$tipo->tipo?>: <?=$tipo->nombre?></p>
                <?php endforeach; ?>
                <hr>
                <?php if(!empty($entries[0]['entry_content'])): ?>
                <div class="descripcion">
                    <?=$entries[0]['entry_content']?>
                </div>
                <hr>
                <?php endif; ?>
                <div class="consultar-btn">
                    <a href="<?=base_url()?>productos/" class="volver"><i class="fas fa-caret-square-left"></i></a>
                    <?php $system = $this->configurations->get_configurations('site', 0, get_web_lang()); ?>
                    <a href="https://api.whatsapp.com/send?phone=<?=$system['general_phone']?>&text=Hola%20me%20gastar%C3%ADa%20saber%20m%C3%A1s%20informaci%C3%B3n%20sobre: <?=$entries[0]['entry_title']?>,%20gracias" target="_blank" class="consulta">Consultar precio <i class="fab fa-whatsapp"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>