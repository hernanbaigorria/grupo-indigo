<?php $entry_images = explode(',', $entry['entry_images']); ?>
<meta property="og:url" content="<?php echo $entry['url']; ?>" />
<meta property="og:title" content="<?php echo $entry['entry_title'] ?> - Novedades Kunan" />
<meta property="og:type" content="article" />
<meta property="og:locale" content="es_LA" />
<meta property="og:image" content="<?php echo media_uri($entry_images[0]) ?>" />
<meta property="og:description" content="<?php echo $entry['entry_short'] ?>" />
<meta property="og:site_name" content="Kunan" />
<meta property="og:image:width" content="640" />
<meta property="og:image:height" content="480" />