<section class="products" id="productos">
    <div class="image-detalle-01">
        <img src="<?php echo THEME_ASSETS_URL ?>general/img/detalle-01.png" class="img-fluid">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>LO ÚLTIMO EN VEHÍCULOS ELECTRÓNICOS</h3>
            </div>
            <?php $count = ''; foreach($categories as $key => $categoria): ?>
                <?php if(empty($categoria->entry_title)): ?>
                    <div class="col-12 col-sm-3">
                        <div class="next-content">
                            <div class="button-category">
                                <p><?=$categoria->category_name?></p>
                            </div>
                            <div class="next">
                                <p>PRÓXIMAMENTE</p>
                            </div>
                        </div>
                    </div>
                <?php else: ?>
                    <?php $count += count($key); ?>
                    <div class="col-12 col-sm-3">
                        <div class="button-category <?php if($count == 1): ?> active <?php endif; ?>" data-category="<?=$categoria->category_slug?>">
                            <p><?=$categoria->category_name?></p>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="boxes">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-2"></div>
                <div class="col-12 col-sm-8">
                    <div id="products" class="owl-carousel">
                    </div>
                </div>
                <?php if(false): ?>
                <div class="col-12 col-sm-4">
                    <div class="box-card-product">
                        <div class="content-card">
                            <div class="card-product-image">
                                <img src="<?php echo THEME_ASSETS_URL ?>general/img/product.png" class="img-fluid">
                                <div class="caracts">
                                    <div class="info-left">
                                        <p>Colores disponibles:</p>
                                        <div class="color"></div>
                                    </div>
                                    <div class="btn-ver-mas">VER MÁS ></div>
                                </div>
                            </div>
                            <div class="card-info-product">
                                <h3>BT<br>9400</h3>
                                <p>
                                    <strong>Tipo de producto:</strong> Monopatín eléctrico<br>
                                    <strong>Material:</strong> Aleación de aluminio<br>
                                    <strong>Motor:</strong> 350 W<br>
                                    <strong>Neumáticos:</strong> 10 pulgadas<br>
                                    <strong>Batería:</strong> 36W 10,4Ah<br>
                                    <strong>Peso máximo admitido:</strong> 200 Kg<br>
                                    <strong>Tiempo de carga:</strong> 3 a 5 Hs.<br>
                                    <strong>Velocidad máxima:</strong> 30/35 Km/h<br>
                                    <strong>Autonomía:</strong> 25 a 30 Km<br>
                                    <strong>Medidas:</strong> Largo 1120 mm - Ancho 500mm - Altura: 1140 mm<br>
                                    <strong>Peso:</strong> 17 Kg

                                </p>
                                <div class="w-100">
                                    <div class="content-arrow volver-card">
                                        <i class="fa-solid fa-chevron-left"></i>
                                    </div>
                                    <div class="btn-pedir">
                                        <a href="#contacto">PEDILO ></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-4">
                    
                </div>
                <?php endif; ?>
                <div class="col-12 col-sm-2"></div>
            </div>
        </div>
    </div>
</section>