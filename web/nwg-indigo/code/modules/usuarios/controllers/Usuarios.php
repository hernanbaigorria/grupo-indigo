<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends MX_Controller {

	// System 

		private $current_category 	= FALSE;
		private $current_entry 		= FALSE;

	    function __construct() {
	        parent::__construct();
	       	$this->load->model('mod_usuarios');
	    }

		public function _remap( $method )
		{
			$segs 	= $this->uri->segment_array();
	        $params	= array_slice($segs, array_search($method, $segs));

	        // Bloqueamos determinados metodos.
		    if (in_array($method, array('module_managment'))) 
		    	show_404();
		    else
		    	if (method_exists($this, $method))
		    		$this->$method(implode(',', $params));
		    	else {
		    		$cond['category_slug'] 	= $method;
		    		$categories 			= $this->mod_productos_categories->get_categories($cond, FALSE, 0, 500);
		    		if (count($categories)  > 0) {
		    			$this->current_category = $categories[0];
		    			$this->render_category();
		    		}
		    		else
		    			show_404();
		    	}
		}

	// Public Access

	    // Pantalla publica principal del modulo.
		public function index()
		{
			$page 	 	= (int)$this->input->get('pagina');
			$page 	 	= ($page > 0) ? $page - 1 : 0;
			
			// Si tenemos alguna pagina indicada por URL, vamos derecho alla, sino mostramos el listado de la categoria.
			$cond['entry_published'] 				= 1;
			$cond['entry_date<='] 					= date('Y-m-d H:i:s');
			$entries 								= $this->mod_usuarios->get_entries($cond, FALSE, $page, 10, array('entry_date' => 'DESC'), $this->input->get('buscar'));
			
			$module_data['entries'] 		= $entries;
			$module_data['categories'] 		= $this->mod_productos_categories->get_categories(FALSE, FALSE, 0, 500);
			$module_data['pagination_current']		= $page;
			$module_data['pagination_total_items']	= @$module_data['entries'][0]['total_results'];
			$module_data['pagination_per_page']		= 10;

			$data['MODULE_BODY'] 	= $this->load->view('public/listing', $module_data, TRUE);
			echo minify_html(render_module('novedades', $data));
		}

		public function module_managment()
		{
			$this->control_library->session_check();
			
			// Levantamos variables GET
			$action 	= $this->input->get('action');
			$id_entry 	= $this->input->get('id');
			$page 	 	= (int)$this->input->get('pagina');
			$page 	 	= ($page > 0) ? $page - 1 : 0;

			switch ($action) {
				case 'edit':
					$data['user'] 		= $this->mod_usuarios->get_user($id_entry);
					$this->load->view('manager/user_editor', $data);
					break;
				case 'new':
					$data = '';
					$this->load->view('manager/user_creator', $data);
					break;
				default:
					$data['entries'] = $this->mod_usuarios->get_usuarios(FALSE, FALSE, $page, 10,FALSE);
					
					$data['pagination_current']		= $page;
					$data['pagination_total_items']	= @$data['entries'][0]['total_results'];
					
					$data['pagination_per_page']	= 10;
					$data['PAGE_BUTTON']['link']	= '?action=new';
					$data['PAGE_BUTTON']['class']	= 'green';
					$data['PAGE_BUTTON']['icon']	= 'fa-plus';
					$data['PAGE_BUTTON']['text']	= 'Nuevo usuario';

					
					$this->load->view('manager/main', $data);
					break;
			}
		}


		public function email()
		{
			
			print_r("expression");
			exit;
		}

		public function send()
		{	
			$producto = '';
			$utm_source = '';
			$utm_medium = '';
			$utm_campaign = '';
			$utm_term = '';
			$utm_content = '';
			if(isset($_POST['producto'])): $producto = $_POST['producto']; endif;
			if(isset($_GET['utm_source'])): $utm_source = $_GET['utm_source']; endif;
			if(isset($_GET['utm_medium'])): $utm_medium = $_GET['utm_medium']; endif;
			if(isset($_GET['utm_campaign'])): $utm_campaign = $_GET['utm_campaign']; endif;
			if(isset($_GET['utm_term'])): $utm_term = $_GET['utm_term']; endif;
			if(isset($_GET['utm_content'])): $utm_content = $_GET['utm_content']; endif;

			$data = array(

				'nombre' => $_POST['nombre'],
				'telefono' => $_POST['telefono'],
				'email' => $_POST['email'],
				'mensaje' => $_POST['mensaje'],
				'estado' => 'ingresado',
				'producto' => $producto,
				'utm_source' => $utm_source,
				'utm_medium' => $utm_medium,
				'utm_campaign' => $utm_campaign,
				'utm_term' => $utm_term,
				'utm_content' => $utm_content,
				'date' => date('Y-m-d')
			);

			$this->db->insert('contactos', $data);

			$this->load->library('email');
			$this->email->initialize();
			$this->email->from("burteo.vehiculoselectricos@burteo.com.ar", "Burteo");
			$this->email->to("burteo.vehiculoselectricos@gmail.com");
			$this->email->subject(mb_convert_encoding("Nuevo contacto web", "UTF-8")); 

			$html  = '';
			if(empty($_POST['producto'])):
				$html .= '
				<b>Nombre:</b> '.$_POST['nombre'].'<br>
				<b>Teléfono:</b> '.$_POST['telefono'].'<br>
				<b>Email:</b> '.$_POST['email'].'<br>
				<b>Mensaje:</b> '.$_POST['mensaje'].'<br>
				';
			else:
				$html .= '
				<b>Nombre:</b> '.$_POST['nombre'].'<br>
				<b>Teléfono:</b> '.$_POST['telefono'].'<br>
				<b>Email:</b> '.$_POST['email'].'<br>
				<b>Mensaje:</b> '.$_POST['mensaje'].'<br>
				<b>Producto de interes:</b> '.$producto.'<br>
				';
			endif;       
			$this->email->message(mb_convert_encoding($html, "UTF-8"));
			$this->email->set_mailtype("html");
			@$this->email->send();

			redirect('gracias');
		}

}
