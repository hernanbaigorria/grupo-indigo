<section class="nosotros" id="nosotros">
    <div class="image-detalle-02">
        <img src="<?php echo THEME_ASSETS_URL ?>general/img/detalle-02.png" class="img-fluid">
    </div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 col-sm-4 p-0">
                <img src="<?php echo THEME_ASSETS_URL ?>general/img/image-nosotros.png" class="img-fluid">
            </div>
            <div class="col-12 col-sm-1"></div>
            <div class="col-12 col-sm-7 p-0">
                <div class="content-title">
                    <?php gtp_paragraph('titulo-01', 'span', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                    <?php gtp_paragraph('titulo-02', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                </div>
                <?php gtp_html('free-content', 'div', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            </div>
        </div>
    </div>
</section>