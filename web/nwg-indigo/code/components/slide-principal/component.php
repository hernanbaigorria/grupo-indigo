<?php #debugger($COMPONENT_CFG); ?>

    <?php $slider_images = explode(',' , $COMPONENT_CFG['img_slider']) ?>
<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel" data-bs-interval="4000" data-bs-pause="false">
  <?php if (!$EDITABLE): ?>
  <div class="carousel-indicators" >
    <?php foreach ($slider_images as $_kimages=> $_id_images): ?>
      <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="<?=$_kimages?>" class="<?php if($_kimages == 0): echo "active"; endif;?>"   aria-current="true" aria-label="Slide 1"></button>
    <?php endforeach; ?>
  </div>
  <?php endif; ?>
    <div class="carousel-inner">
      <?php if($COMPONENT_CFG['img_slider']): ?>
              <?php 
              $counter = 0;
              foreach ($slider_images as $_kimages=> $_id_images): ?>
              <?php if (!$EDITABLE): ?><div class="carousel-item <?php if($_kimages == 0): echo "active"; endif;?>"><?php endif; ?>
                <div class="image-slide <?php echo ($counter % 2 == 0) ? '' : 'otro-color'; ?>" style="background-image:url('<?php echo media_uri($_id_images) ?>');">
                  <div class="w-100">
                    <div class="row">
                      <div class="col-12">
                        <div class="content-text">
                          <?php gtp_paragraph('titulo-01'.$_id_images, 'h4', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                          <?php if(!empty(gtp_get_content('link_de_caja'.$_id_images, $ID_COMPONENT, $LANG))): ?>
                            <a href="<?php echo gtp_get_content('link_de_caja'.$_id_images, $ID_COMPONENT, $LANG) ?>">Ver más</a>
                          <?php endif; ?>
                          <?php if ($EDITABLE): ?>
                              <div class="co-12" style="color:#fff;margin-top:30px;">
                                <label>Link de caja:</label>
                                <?php gtp_paragraph('link_de_caja'.$_id_images, 'span', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                              </div>
                            <?php endif; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php if (!$EDITABLE): ?></div><?php endif; $counter++; ?>
          <?php endforeach ?>
      <?php endif; ?>
    </div>
    <?php if (!$EDITABLE): ?>
    <button class="carousel-control-prev" style="display:none;" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" style="display:none;" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  <?php endif; ?>
</div>