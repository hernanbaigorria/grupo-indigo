<section class="registro">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-3"></div>
            <div class="col-12 col-md-5">
                <form class="rgs" method="post" action="<?=base_url()?>s/registro/">
                    <input type="text" name="nombre" placeholder="Nombre y Apellido" required>
                    <input type="text" name="celular" placeholder="Celular" required>
                    <input type="text" name="razon_social" placeholder="Razón social" required>
                    <hr>
                    <input type="email" name="email" placeholder="E-mail" required>
                    <input type="password" name="password" placeholder="Contraseña" required>
                    <input type="submit" value="Ingresar">
                    <a href="<?=base_url()?>registro">No tengo usuario, quiero crear uno!</a>
                    <div class="user-existe">
                        <div class="close-modal">x</div>
                        <h3>El correo ingresado ya se encuentra registrado</h3>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-3"></div>
        </div>
    </div>
</section>