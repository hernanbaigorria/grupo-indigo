<style type="text/css">
    footer {padding: 0;background: #000;}
    footer .col-12.col-sm-6 {display:none;}
    footer .copy {margin:50px 0px;}
</style>
<section class="gracias">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>¡Gracias por tu interés!</h3>
                <p>Pronto se comunicará un asesor para brindarte la mejor experiencia.</p>
            </div>
        </div>
    </div>
</section>