<section class="login">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-12 col-md-3"></div>
            <div class="col-12 col-md-5">
                <form class="log" method="post" action="<?=base_url()?>s/login/">
                    <input type="email" name="email" placeholder="E-mail">
                    <input type="password" name="password" placeholder="Contraseña">
                    <input type="submit" value="Ingresar">
                    <div class="user-existe">
                        <div class="close-modal">x</div>
                        <h3>El correo ingresado ya se encuentra registrado</h3>
                    </div>
                </form>
            </div>
            <div class="col-12 col-md-3"></div>
        </div>
    </div>
</section>