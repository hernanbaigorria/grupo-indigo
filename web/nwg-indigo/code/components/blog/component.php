<section class="blog" id="blog">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h3>BLOG</h3>
            </div>
        </div>
    </div>
    <div class="w-100 content-card-blog">
        <div class="container">
            <div class="row">
                <?php $blog = explode(',' , $COMPONENT_CFG['card_blog']) ?>
                <?php foreach ($blog as $_kblog=> $blogcard): ?>
                    <div class="col-12 col-sm-4">
                        <div class="card-blog">
                            <div class="content-img" style="background-image:url('<?php echo media_uri($blogcard) ?>')"></div>
                            <div class="title-blog">
                                <?php gtp_paragraph('title-blog'.$blogcard, 'h4', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                            </div>
                            <div class="description-blog">
                                <?php gtp_html('content'.$blogcard, 'div', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <?php if(false): ?>
                <div class="col-12 col-sm-4">
                    <div class="card-blog">
                        <div class="content-img" style="background-image:url('<?php echo THEME_ASSETS_URL ?>general/img/blog-image-06.jpg')"></div>
                        <div class="title-blog">
                            <h4>SUSTENTABILIDAD</h4>
                        </div>
                        <div class="description-blog">
                            <p>¿Por qué los scooters y monopatines eléctricos son más sostenibles que los vehículos a gasolina?<br><br>
                                • No usan combustible. <br>
                            • Reducen emisiones de carbono.<br>
                            • Ahorran energía.<br>
                            • Requieren menos mantenimiento.<br>
                            • No generan contaminación acústica.</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-4">
                    <div class="card-blog">
                        <div class="content-img" style="background-image:url('<?php echo THEME_ASSETS_URL ?>general/img/blog-image-07.jpg')"></div>
                        <div class="title-blog">
                            <h4>OPTIMIZÁ LA BATERÍA</h4>
                        </div>
                        <div class="description-blog">
                            <p>¿Por qué los scooters y monopatines eléctricos son más sostenibles que los vehículos a gasolina?<br><br>
                                • No usan combustible. <br>
                            • Reducen emisiones de carbono.<br>
                            • Ahorran energía.<br>
                            • Requieren menos mantenimiento.<br>
                            • No generan contaminación acústica.</p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-4">
                    <div class="card-blog">
                        <div class="content-img" style="background-image:url('<?php echo THEME_ASSETS_URL ?>general/img/blog-image-05.jpg')"></div>
                        <div class="title-blog">
                            <h4>COMUNIDAD BURTEO</h4>
                        </div>
                        <div class="description-blog">
                            <p>¿Por qué los scooters y monopatines eléctricos son más sostenibles que los vehículos a gasolina?<br><br>
                                • No usan combustible. <br>
                            • Reducen emisiones de carbono.<br>
                            • Ahorran energía.<br>
                            • Requieren menos mantenimiento.<br>
                            • No generan contaminación acústica.</p>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>