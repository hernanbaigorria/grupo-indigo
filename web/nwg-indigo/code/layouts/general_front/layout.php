<!DOCTYPE html>
<html dir="ltr" lang="es">
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
        <meta name="description" content=" " />
        <meta name="keywords" content=" " />

        <title><?=$PAGE_CFG['site_title']?></title>

        <?php if ($EDITABLE): ?>
            <?php echo $EDITOR_HEADER ?>
            <?php echo $EDITOR_FOOTER ?>
        <?php endif ?>

        <!-- Favicon and Touch Icons -->
        <link href="<?=media_uri($PAGE_CFG['page_favicon'])?>" rel="shortcut icon">
        
        <!-- Stylesheet -->

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;500;700;800;900&display=swap" rel="stylesheet">

        <link href="<?php echo THEME_ASSETS_URL ?>general/css/jquery-ui.min.css" rel="stylesheet" type="text/css">

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css'>

        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css'>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/bootstrap.min.css" rel="stylesheet" type="text/css">

        
        <link href="<?php echo THEME_ASSETS_URL ?>general/css/custom.css?v=<?=time()?>" rel="stylesheet" type="text/css">
        <style type="text/css">
            <?php echo $PAGE['page_custom_css'] ?>
        </style>
    </head>
    <body>
            <?php if ($EDITABLE): ?>
                <style type="text/css">
                    .image-detalle-01, .image-detalle-02, footer {display:none;}
                    .asesoramiento {margin:0;background:#000;padding:80px 0px;}
                    .blog {padding:80px 0px;}
                    .card-asosa .content-01, .card-asosa .content-02 {display:block;}
                </style>
            <?php endif; ?>
            <?php if (!$EDITABLE): ?>
            <header>
                <div class="container">
                    <nav class="navbar navbar-expand-lg navbar-light">
                      <div class="container-fluid">
                        <div class="content">
                            <a class="navbar-brand" href="<?=base_url()?>">
                                <img src="<?=media_uri($PAGE_CFG['logo_header'])?>" class="img-fluid">
                            </a>
                        </div>
                        <div class="content hidden-xs">
                            <form method="get" action="<?=base_url()?>productos/" class="form-search hidden-xs">
                                <?php
                                $b_value = isset($_GET['b']) ? htmlspecialchars($_GET['b']) : '';
                                ?>
                                <input type="text" name="b" placeholder="Buscar" value="<?= $b_value ?>">
                                <i class="fas fa-search"></i>
                            </form>
                        
                        </div>
                        <div class="content">
                            <div class="collapse navbar-collapse" id="navbarNav">
                              <ul class="navbar-nav">
                                <?php if(null !== $this->session->userdata('usuario')):
                                    $stored_data = $this->session->userdata('usuario');
                                ?>
                                    <div class="dropdown">
                                      <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        Hola <?=$stored_data[0]['nombre']?>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                        <li><a class="dropdown-item" href="<?=base_url()?>s/loginout/">Salir</a></li>
                                      </ul>
                                    </div>
                                <?php else: ?>
                                    <?php 
                                        for ($i=1; $i <= 5; $i++) { 
                                        ?>
                                        <?php $gtp_pages = explode(',', $PAGE_CFG['page_main_menu_opts'.$i]) ?>
                                        <?php if (!empty($PAGE_CFG['page_main_menu_name'.$i])): ?>
                                            <li class="nav-item">
                                                <?php 
                                                    $this_url = page_uri($PAGE_CFG['page_main_menu_link'.$i], $EDITABLE); 
                                                    $target = (strpos($this_url, base_url()) === FALSE) ? '_blank' : '';
                                                ?>
                                                <a class="nav-link" href="<?php echo $this_url ?>"><?php echo $PAGE_CFG['page_main_menu_name'.$i] ?></a>
                                            </li> 
                                        <?php endif ?>
                                        <?php 
                                        }
                                    ?>
                                <?php endif; ?>
                                
                              </ul>
                            </div>
                        </div>

                        <div class="content visible-xs" style="flex: 1;margin-top: 10px;">
                            <form method="get" action="<?=base_url()?>productos/" class="form-search">
                                <?php
                                $b_value = isset($_GET['b']) ? htmlspecialchars($_GET['b']) : '';
                                ?>
                                <input type="text" name="b" placeholder="Buscar" value="<?= $b_value ?>">
                                <i class="fas fa-search"></i>
                            </form>
                        
                        </div>
                      </div>
                    </nav>
                </div>
            </header>
            <?php endif; ?>

            <!-- Start main-content -->
            <div class="page_components_containers">
                <?php render_components($ID_PAGE, $LANG, $VERSION, $EDITABLE) ?>
            </div>

            <?php echo @$MODULE_BODY; ?>
            <!-- end main-content -->
            
            <?php if (!empty($PAGE_CFG['general_phone'])): ?>
            <div class="btn-wpp">
                <a href="https://api.whatsapp.com/send?phone=<?=$PAGE_CFG['general_phone']?>" target="_blank">
                    <i class="fab fa-whatsapp"></i>
                </a>
            </div>
            <?php endif; ?>

            <footer id="contacto">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12 col-sm-3" style="margin-bottom:auto;">
                            <h3>CONTACTO</h3>
                            <?php if (!empty($PAGE_CFG['general_mail'])): ?>
                                <a href="mailto:<?=$PAGE_CFG['general_mail']?>" class="other-enlaces">
                                    <i class="fas fa-envelope"></i>
                                    <p><?=$PAGE_CFG['general_mail']?></p>
                                </a>
                            <?php endif; ?>
                            <?php if (!empty($PAGE_CFG['general_phone'])): ?>
                                <a href="https://api.whatsapp.com/send?phone=<?=$PAGE_CFG['general_phone']?>" class="other-enlaces">
                                    <i class="fab fa-whatsapp"></i>
                                    <p><?=$PAGE_CFG['general_phone']?></p>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="col-12 col-sm-3" style="margin-bottom:auto;">
                            <?php if (!empty($PAGE_CFG['general_address'])): ?>
                                <h3>Ubicación</h3>
                                <a href="<?php echo page_uri($PAGE_CFG['general_address']) ?>" target="_blank" class="other-enlaces">
                                    <i class="fas fa-map-marker"></i>
                                    <p><?php echo page_title($PAGE_CFG['general_address'], $LANG) ?></p>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div class="col-12 col-sm-3"></div>
                        <div class="col-12 col-sm-3" style="margin-bottom:auto;">
                            <div class="content-redes">
                                <img src="<?php echo THEME_ASSETS_URL ?>general/img/footer-logo.png" class="img-fluid">
                                <div class="redes-links">
                                    <?php if (!empty($PAGE_CFG['page_social_facebook'])): ?>
                                        <a href="<?php echo page_uri($PAGE_CFG['page_social_facebook']) ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <?php endif; ?>
                                    <?php if (!empty($PAGE_CFG['page_social_instagram'])): ?>
                                        <a href="<?php echo page_uri($PAGE_CFG['page_social_instagram']) ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <div class="col-12 text-center p-0">
                <p class="copy" style="margin: 0;color: #fff;background: #000;padding: 15px 0px;font-size: 13px;">Copyright © <?=date('Y')?> Grupo indigo<br><small>Desarrollado por <a href="https://www.linkedin.com/in/hernanbaigorria/" target="_blank" style="color:#fff;text-decoration:underline;">Hernan Baigorria</a></small></p>
            </div>

        <script type="text/javascript">
            <?php // echo $PAGE['page_custom_js'] ?>
        </script>




        <script type="text/javascript" src="<?php echo THEME_ASSETS_URL ?>general/js/custom.js"></script>
        <?php if (!$EDITABLE): ?>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            
            
        <?php endif ?>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js'></script>

        <script type="text/javascript">
            function clearAllFilters() {
                const radios = document.querySelectorAll('input[type="radio"]');
                radios.forEach(radio => {
                    radio.checked = false;
                });

                // Redireccionar a la misma página sin parámetros de búsqueda
                const newUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                window.location.href = newUrl;
            }
        </script>
          <script type="text/javascript">
            $(".rgs").submit(function(event){
                event.preventDefault(); //prevent default action 
                var post_url = $(this).attr("action"); //get form action url
                var request_method = $(this).attr("method"); //get form GET/POST method
                var form_data = $(this).serialize(); //Encode form elements for submission
                
                $.ajax({
                    url : post_url,
                    type: request_method,
                    data : form_data,
                    beforeSend: function() {
                        // setting a timeout
                        $('.enviar').addClass('disable');
                        $('.gif').show('slow');
                        $('.message').hide("slow");
                    },
                    success: function(response){
                        if(response == 'existe'){
                            $('.user-existe').addClass('abrir');
                        }
                    }
                });
            });

            $(".log").submit(function(event){
                event.preventDefault(); //prevent default action 
                var post_url = $(this).attr("action"); //get form action url
                var request_method = $(this).attr("method"); //get form GET/POST method
                var form_data = $(this).serialize(); //Encode form elements for submission
                
                $.ajax({
                    url : post_url,
                    type: request_method,
                    data : form_data,
                    beforeSend: function() {
                        // setting a timeout
                        $('.enviar').addClass('disable');
                        $('.gif').show('slow');
                        $('.message').hide("slow");
                    },
                    success: function(response){
                        if(response == 'no-password'){
                            $('.user-existe').addClass('abrir');
                            $('.user-existe h3').text('La contraseña ingresada es incorrecta');
                        }
                        if(response == 'no-email'){
                            $('.user-existe').addClass('abrir');
                            $('.user-existe h3').text('El correo no es valido o no se encuentra registrado');
                        }

                        if(response == 'entro'){
                            window.location.replace("<?php echo base_url() ?>");
                        }
                    }
                });
            });

            $( ".close-modal" ).click(function() {
              $('.user-existe').removeClass('abrir');
            });
          </script>
    </body>
</html>