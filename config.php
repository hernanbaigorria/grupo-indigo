<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Nombre del tema
|--------------------------------------------------------------------------
|
| Debe ser el nombre de la carpeta en la que se encuentra el tema a mostrar.
| Los temas deben estar en /application/views/themes/
|
*/
define('GESTORP_THEME', 'nwg-indigo');

/*
|--------------------------------------------------------------------------
| Presentación del gestor de contenidos
|--------------------------------------------------------------------------
|
| GESTORP_SIGNATURE: Es la titulo.
| GESTORP_SIGNATURE: Es la firma que tendra el gestor de contenido.
|
*/
define('GESTORP_NAME', 'Gestor de Contenidos');
define('GESTORP_SIGNATURE', '');

/*
|--------------------------------------------------------------------------
| Base de datos
|--------------------------------------------------------------------------
|
| Las siguientes constantes definen los datos de conexion a la DB.
|
*/

define('DB_HOST', 		'localhost'); 			// DB host
define('DB_USERNAME', 	'nwg_indigo');			// DB user
define('DB_PWD', 		'AF]a$3?X~xuS');			// DB password
define('DB_NAME', 		'nwg_indigo');			// DB name

/*
define('DB_HOST', 		'localhost'); 			// DB host
define('DB_USERNAME', 	'root');			// DB user
define('DB_PWD', 		'linux666');			// DB password
define('DB_NAME', 		'nwg_indigo');			// DB name

/*
|--------------------------------------------------------------------------
| Lenguajes de presentación disponibles al publico.
|--------------------------------------------------------------------------
|
| Los nombres de los lenguajes deben separarse por coma, hay que tener en 
| cuenta que el tema de la web debe estar preparado para multiples leng.
| define('AVAILABLE_WEB_LANGUAGES', 'español,english,russian'); 
|
*/
define('AVAILABLE_WEB_LANGUAGES', 'español');

/*
|--------------------------------------------------------------------------
| Formulario de contacto.
|--------------------------------------------------------------------------
|
| CONTACT_TARGET_EMAILS: Se define las direcciones a la que se enviaran los 
| mensajes de contacto. Separados por coma.
| EMAILS_SENDER_NAME: Nombre de remitente que figurara en los emails.
| EMAILS_DOMAIN: Dominio a traves del cual se enviaran.
| La direccion desde la que salen es info@EMAILS_DOMAIN
|
*/
define('CONTACT_TARGET_EMAILS',	''); 
define('EMAILS_SENDER_NAME', 	'');
define('EMAILS_DOMAIN', 		'');
define('EMAILS_SENDER_ADDR',	'');


/*
|--------------------------------------------------------------------------
| Permisos por defecto
|--------------------------------------------------------------------------
|
| DEFAULT_PERMISSION_VALUE: general.
| DEFAULT_PERMISSION_PAGE_VALUE: Permiso por def para paginas creadas. En 
| caso de ser False, solo el creador podra verlas en el sistema.
| DEFAULT_PERMISSION_MODULE_VALUE: Idem anterior, pero para los modulos que
| estan definidos incialmente.
|
*/
define('DEFAULT_PERMISSION_VALUE', 			FALSE);
define('DEFAULT_PERMISSION_PAGE_VALUE', 	TRUE);
define('DEFAULT_PERMISSION_MODULE_VALUE', 	TRUE);

/*
|--------------------------------------------------------------------------
| Firma del proyecto.
|--------------------------------------------------------------------------
|
| Por respeto a los que trabajamos en este proyecto, agradeceriamos no elimnar
| estas lineas. Si estas continuando nuestro trabajo, sentite libre de firmar aqui tmb.
*/
$GESTORP_SIGNATURE  = 'Nombre del Proyecto'.PHP_EOL;
$GESTORP_SIGNATURE .= 'Año 20XX - Córdoba - Argentina'.PHP_EOL;
$GESTORP_SIGNATURE .= 'Dirección del proyecto: Inglobe (www.inglobe.com.ar);'.PHP_EOL;
$GESTORP_SIGNATURE .= 'Dirección Técnica y Programación: Pablo Chirino (www.peybol.com);';
//$GESTORP_SIGNATURE .= PHP_EOL.'----actualización----'.PHP_EOL;
//$GESTORP_SIGNATURE .= 'Año 20XX - Ciudad - Pais';
//$GESTORP_SIGNATURE .= 'Nuevos desarrollos: tu nombre;';
define('PUBLIC_SIGNATURE', 	$GESTORP_SIGNATURE);